#pragma once

#include "persistent.h"

// #define BLACKBOX_ENABLED

#define BLACKBOX_DATA_SIZE 4 //bytes
// #define BLACKBOX_DATA_COUNT 16384 //* 4 = #bytes 16384
#define BLACKBOX_DATA_COUNT 128 //* 4 = #bytes 16384
#define BLACKBOX_BUFFER_SIZE ((BLACKBOX_DATA_SIZE + 1) * BLACKBOX_DATA_COUNT)

#define BLACKBOX_ID_VOLTIMETER 0

byte blackbox_buffer[BLACKBOX_BUFFER_SIZE];

unsigned blackbox_flash_offset = ff2_persistent_length();
unsigned blackbox_write_pointer = 0;
unsigned ff2_flight_id = 0;
float blackbox_last_time = 0;

void ff2_blackbox_initialize();
void ff2_blackbox_write_float(float value, int id);
void ff2_blackbox_write_int(int value, int id);
void ff2_blackbox_dump();
