#pragma once

#include <malloc.h>
#include <stdlib.h>

#define ff2_pwm(x, y) (analogWrite((x), (y)))
#define ff2_time_micros() (micros())
#define ff2_time_millis() (millis())
#define ff2_time() (ff2_time_micros() / 1000000.0f)
#define ff2_delay(x) (delay((x)))

#define BLUETOOTH_ONTHEFLY_DEBUG
#define USB_ONTHEFLY_DEBUG

#define DEFAULT_SERIAL Serial
#define BLUETOOTH_SERIAL Serial1
#define GPS_SERIAL Serial2

#define INITIALIZATION_TIME 30 //ms

#define ADC_RESOLUTION_TOTAL 1024
#define ADC_MAX_VOLTAGE 3.3f
#define ADC_TO_VOLT(x) ((float) (x) / ADC_RESOLUTION_TOTAL * ADC_MAX_VOLTAGE)
#define ff2_analog_read(x) (analogRead((x)))
#define ff2_read_volts(x) (ADC_TO_VOLT(ff2_analog_read((x))))

#define ON HIGH
#define OFF LOW
#define ff2_make_output(x) (pinMode((x), OUTPUT))
#define ff2_output_mode(x, y) (digitalWrite((x), (y)))

#define ff2_i2c_begin					Wire.begin
#define ff2_i2c_begin_transmission 		Wire.beginTransmission
#define ff2_i2c_write 					Wire.write
#define ff2_i2c_read					Wire.read
#define ff2_i2c_end_transmission		Wire.endTransmission
#define ff2_i2c_request 				Wire.requestFrom
#define ff2_i2c_available				Wire.available

extern char _end;
extern "C" char* sbrk(int i);
char *ramstart = (char*) 0x20070000;
char *ramend = (char*) 0x20088000;

void ff2_printf(char* str, ...)
{	
	const __FlashStringHelper* fmt = F(str);
	char buf[128];
	va_list args;
	va_start(args, fmt);
	#ifdef __AVR__
		vsnprintf_P(buf, sizeof(buf), (const char*) fmt, args);
	#else
		vsnprintf(buf, sizeof(buf), (const char*) fmt, args);
	#endif
	va_end(args);
	DEFAULT_SERIAL.print(buf);
}

void ff2_bluetooth_printf(char* str, ...)
{
	const __FlashStringHelper* fmt = F(str);
	char buf[128];
	va_list args;
	va_start(args, fmt);
	#ifdef __AVR__
		vsnprintf_P(buf, sizeof(buf), (const char*) fmt, args);
	#else
		vsnprintf(buf, sizeof(buf), (const char*) fmt, args);
	#endif
	va_end(args);
	BLUETOOTH_SERIAL.print(buf);
}

void ff2_debug_printf(char* str, ...)
{
	const __FlashStringHelper* fmt = F(str);
	char buf[128];
	va_list args;
	va_start(args, fmt);
	#ifdef __AVR__
		vsnprintf_P(buf, sizeof(buf), (const char*) fmt, args);
	#else
		vsnprintf(buf, sizeof(buf), (const char*) fmt, args);
	#endif
	va_end(args);

	#ifdef BLUETOOTH_ONTHEFLY_DEBUG
		BLUETOOTH_SERIAL.print(buf);
	#endif
	#ifdef USB_ONTHEFLY_DEBUG
		DEFAULT_SERIAL.print(buf);
	#endif
}

unsigned ff2_ram() 
{
	char *heapend = sbrk(0);
	register char* stack_ptr asm("sp");
	struct mallinfo mi = mallinfo();
	// printf("\nDynamic ram used: %d\n", mi.uordblks);
	// printf("Program static ram used %d\n", &_end - ramstart); 
	// printf("Stack ram used %d\n", ramend - stack_ptr);
	// printf("Free memory: %d\n", stack_ptr - heapend + mi.fordblks);
	return stack_ptr - heapend + mi.fordblks;
}
