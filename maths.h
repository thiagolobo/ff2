#pragma once

#include "math.h"

#define FF2_FAST_TRIGONOMETRY
#define FF2_FAST_MATH

#define FF2_PI 		3.14159265358979323f
#define FF2_TAU 	(2.0f * FF2_PI)
#define FF2_EULER 	2.71828182845904523f

#define FF2_DEG_FACTOR (180.0f / FF2_PI)
#define FF2_RAD_FACTOR (FF2_PI / 180.0f)
#define FF2_DEG(x) ((x) * FF2_DEG_FACTOR)
#define FF2_RAD(x) ((x) * FF2_RAD_FACTOR)


#define FF2_SQR(x) ((x) * (x))
#define FF2_POW(a, b) (powf(a, b))
#define FF2_MIN(a, b) ((a) < (b) ? (a) : (b))
#define FF2_MAX(a, b) ((a) > (b) ? (a) : (b))
#define FF2_ABS(x) ((x) >= 0.0f ? (x) : -(x))

#define FF2_QMF_SORT(a, b) {if ((a) > (b)) FF2_QMF_SWAP((a), (b));}
#define FF2_QMF_SWAP(a, b) {float temp = (a); (a) = (b); (b) = temp;}
#define FF2_QMF_COPY(p, v, n) {int i; for (i = 0; i < (n); i++) (p)[i] = (v)[i];}

#define X 0
#define Y 1
#define Z 2

#define ROLL 	X
#define PITCH 	Y
#define YAW 	Z

typedef struct ff2_vector3f_def
{
	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;
} ff2_vector3f;

typedef struct ff2_angles_def
{
	float roll = 0.0f;
	float pitch = 0.0f;
	float yaw = 0.0f;
} ff2_angles;

typedef struct ff2_quaternion_def
{
	float w = 1.0f;
	float x = 0.0f;
	float y = 0.0f;
	float z = 0.0f;
} ff2_quaternion;

int ff2_constrain(int amount, int low, int high);
float ff2_constrainf(float amount, float low, float high);

int ff2_apply_deadband(int value, int deadband);
float ff2_apply_deadbandf(float value, float deadband);

int ff2_range_map(int x, int srcMin, int srcMax, int destMin, int destMax);
float ff2_range_mapf(float x, float srcMin, float srcMax, float destMin, float destMax);

float ff2_sin(float x);
float ff2_cos(float x);
float ff2_tan(float x);
float ff2_atan2(float y, float x);
float ff2_acos(float x);

float ff2_inv_sqrt(float x);
float ff2_sqrt(float x);

void ff2_normalize(ff2_vector3f* src, ff2_vector3f* dest);
float ff2_magnitude(ff2_vector3f vector);

void ff2_rotation_matrix(ff2_angles angles, float matrix[3][3]);
void ff2_rotate_vector(ff2_angles angles, ff2_vector3f* vector);
void ff2_rotate_vector(float matrix[3][3], ff2_vector3f* vector);
// void ff2_rotate_vector(ff2_quaternion angles, ff2_vector3f* vector);
// void ff2_hamilton_product(ff2_quaternion a, ff2_quaternion b, ff2_quaternion* result);

float ff2_first_order_LPF_time_constant(float cutoff_frequency);

float ff2_quick_median_filter_3(float* vector);
float ff2_quick_median_filter_5(float* vector);
float ff2_quick_median_filter_7(float* vector);
float ff2_quick_median_filter_9(float* vector);

void ff2_apply_median_filter_3(float* source, float* destination, int length);
void ff2_apply_median_filter_5(float* source, float* destination, int length);
void ff2_apply_median_filter_7(float* source, float* destination, int length);
void ff2_apply_median_filter_9(float* source, float* destination, int length);

void maths_debug();
