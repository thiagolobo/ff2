#include "profiler.h"

void ff2_profiler_start()
{
    profiler_count = 0;
    profiler_accumulated = 0;
    profiler_last_time = ff2_time_micros();
}

void ff2_profiler_update()
{
    unsigned long current_time = ff2_time_micros();
    profiler_count++;
    profiler_accumulated += current_time - profiler_last_time;
    profiler_last_time = current_time;
}

void ff2_profiler_end()
{
    ff2_printf(">> Profiler finished!\n");
    ff2_printf("Average Time (us): %f\n", ((float) profiler_accumulated / profiler_count));
    ff2_printf("Average Time  (s): %f\n", ((float) profiler_accumulated / profiler_count) / 1000000.0f);
}
