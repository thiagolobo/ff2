#pragma once

int* i2cd_output_6x;

void ff2_i2cd_initialize();
void ff2_i2cd_write(int device_address, byte register_address, byte value);
// void ff2_i2cd_write(int device_address, byte base_register_address, unsigned length, int* vector);
void ff2_i2cd_read(int device_address, byte base_register_address, unsigned length, int* vector);
void ff2_i2cd_read(int device_address, byte register_address, int* value);