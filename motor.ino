#include "motor.h"

void ff2_motor_initialize()
{
	ff2_delay(INITIALIZATION_TIME);
	ff2_debug_printf(">> Initializing Motors...\n");
	
	ff2_motor front_left;
	front_left.port = MOTOR_FRONT_L_PORT;
	front_left.alias = MOTOR_FRONT_L_ALIAS;
	front_left.throttle = MOTOR_MIN_THROTTLE;
	ff2_motor_configure(&front_left);

	ff2_motor front_right;
	front_right.port = MOTOR_FRONT_R_PORT;
	front_right.alias = MOTOR_FRONT_R_ALIAS;
	front_right.throttle = MOTOR_MIN_THROTTLE;
	ff2_motor_configure(&front_right);

	ff2_motor back_left;
	back_left.port = MOTOR_BACK_L_PORT;
	back_left.alias = MOTOR_BACK_L_ALIAS;
	back_left.throttle = MOTOR_MIN_THROTTLE;
	ff2_motor_configure(&back_left);

	ff2_motor back_right;
	back_right.port = MOTOR_BACK_R_PORT;
	back_right.alias = MOTOR_BACK_R_ALIAS;
	back_right.throttle = MOTOR_MIN_THROTTLE;
	ff2_motor_configure(&back_right);

	motors[MOTOR_FRONT_L] = front_left;
	motors[MOTOR_FRONT_R] = front_right;
	motors[MOTOR_BACK_L] = back_left;
	motors[MOTOR_BACK_R] = back_right;
}

void ff2_motor_configure(ff2_motor* motor)
{
	PIO_Configure(g_APinDescription[motor->port].pPort,
                  PIO_PERIPH_B,
                  g_APinDescription[motor->port].ulPin,
                  g_APinDescription[motor->port].ulPinConfiguration);

	pmc_enable_periph_clk(ID_PWM);
	PWMC_ConfigureClocks(1000000, 0, VARIANT_MCK);
	
	PWMC_ConfigureChannel(PWM, motor->alias, PWM_CMR_CPRE_CLKA, 0, 0);
  	PWMC_SetPeriod(PWM, motor->alias, 3333); // 300 Hz  	
  	PWMC_EnableChannel(PWM, motor->alias);
  	PWMC_SetDutyCycle(PWM, motor->alias, ff2_range_map(motor->throttle, 0, 4095, 0, 3333));
}

void ff2_motor_set_throttle(byte motor, int throttle)
{
	ff2_motor* current = &motors[motor];
	current->throttle = (throttle > MOTOR_MAX_THROTTLE) ? MOTOR_MAX_THROTTLE : (throttle < MOTOR_MIN_THROTTLE ? MOTOR_MIN_THROTTLE : throttle);
	PWMC_SetDutyCycle(PWM, current->alias, ff2_range_map(current->throttle, 0, 4095, 0, 3333));
}

// int value = (int) ff2_range_mapf(radio_chan_value(THROTTLE), 0.0f, 1.0f, MOTOR_MIN_THROTTLE - 100, MOTOR_MAX_THROTTLE);
// motor_set_throttle(MOTOR_FRONT_R, value);
// motor_set_throttle(MOTOR_FRONT_L, value);
// motor_set_throttle(MOTOR_BACK_R, value);
// motor_set_throttle(MOTOR_BACK_L, value);
