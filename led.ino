#include "led.h"

void ff2_led_initialize()
{
	ff2_delay(INITIALIZATION_TIME);
	ff2_debug_printf(">> Initializing LEDs...\n");
	
	float time = ff2_time();

	ff2_led blue;
	blue.port = LED_BLUE_PORT;
	blue.last_time = time;
	
	ff2_led orange;
	orange.port = LED_ORANGE_PORT;
	orange.last_time = time;
	
	ff2_led green;
	green.port = LED_GREEN_PORT;
	green.last_time = time;
	
	ff2_led red;
	red.port = LED_RED_PORT;
	red.last_time = time;

	led_array[LED_BLUE] = blue;
	led_array[LED_ORANGE] = orange;
	led_array[LED_GREEN] = green;
	led_array[LED_RED] = red;

	ff2_led_debug();
}

void ff2_led_update()
{
	float time = ff2_time();

	for (byte i = 0; i < LED_MAX_COUNT; i++)
	{
		ff2_led* current = &led_array[i];

		if (current->mode == LED_BLINK)
		{
			if (current->state)
		    {		    	
		      	if (time - current->last_time >= current->blink_period_low) 
		      	{
		        	ff2_pwm(current->port, 255 * led_scale);
		        	current->state = false;
		        	current->last_time = time;
		 		}
			} 
			else 
			{
				if (time - current->last_time >= current->blink_period_high) 
				{
	 				ff2_pwm(current->port, 0);
	 				current->state = true;
	 				current->last_time = time;
	 			}
			}
		}
		else
		{
			current->last_time = time;
		}		
	}
}

void ff2_led_set_fixed(byte led, byte brightness)
{
	ff2_led* current = &led_array[led];
	current->mode = LED_FIXED;
	current->brightness = brightness;
	ff2_pwm(current->port, brightness * led_scale);
}

void ff2_led_set_blink(byte led, float blink_period_low, float blink_period_high)
{
	ff2_led* current = &led_array[led];
	current->mode = LED_BLINK;
	current->blink_period_low = blink_period_low;
	current->blink_period_high = blink_period_high;
}

void ff2_led_debug()
{
	ff2_debug_printf(">> Initializing LED debug...\n");

	ff2_led_set_fixed(LED_BLUE, 255 * led_scale);
	ff2_led_set_fixed(LED_ORANGE, 255 * led_scale);
	ff2_led_set_fixed(LED_GREEN, 255 * led_scale);
	ff2_led_set_fixed(LED_RED, 255 * led_scale);

	delay(8000);

	ff2_led_set_fixed(LED_BLUE, 0);
	ff2_led_set_fixed(LED_ORANGE, 0);
	ff2_led_set_fixed(LED_GREEN, 0);
	ff2_led_set_fixed(LED_RED, 0);

}

void ff2_led_set_scale(float value)
{
	led_scale = (value > 1.0f) ? 1.0f : ((value < 0.0f) ? 0.0f : value);
}
