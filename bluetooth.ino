#include "bluetooth.h"

void ff2_bluetooth_initialize()
{	
	ff2_delay(INITIALIZATION_TIME);
	BLUETOOTH_SERIAL.begin(57600);
	ff2_debug_printf(">> Initializing Bluetooth Interface...\n");

	ff2_make_output(BLUETOOTH_PIN_GND);
	ff2_output_mode(BLUETOOTH_PIN_GND, OFF);
}

void ff2_bluetooth_update()
{
	byte index = 0;

	while (BLUETOOTH_SERIAL.available() > 0)
    {
    	delay(3);
    	if (index < BLUETOOTH_INPUT_SIZE - 1)
		{
			bluetooth_input_string[index++] = BLUETOOTH_SERIAL.read();
			bluetooth_input_string[index] = '\0';
		}
		else
		{
			ff2_printf("Stuck in Bluetooth read loop. Breaking out.\n");
			index = 0;
			break;
		}
	}

	if (index > 0)
	{		
		ff2_bluetooth_parse(bluetooth_input_string);
	}
}

bool ff2_bluetooth_parse(char* message)
{
	unsigned message_id = -1;
	char* payload = strchr(message, ' ');
	float float_value = 0.0f;
	int int_value = 0;
	ff2_pid_data* pid = 0;

	if (sscanf(message, "%i %*s", &message_id) <= 0)
	{
		return false;
	} 

	ff2_debug_printf("Received message #%i!\n", message_id);
	switch (message_id)
	{
		case BLUETOOTH_ID_SANITY_CHECK:
			ff2_debug_printf("Not insane!\n");
			return true;
			break;
		case BLUETOOTH_ID_VOLTIMETER_THRESHOLD:
			ff2_debug_printf("Setting voltimeter threshold...\n");
			float_value = voltimeter_threshold;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				ff2_voltimeter_set_threshold(float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_VOLTIMETER_BBP:
			ff2_debug_printf("Setting voltimeter BBP...\n");
			int_value = voltimeter_bbp;
			if (ff2_bluetooth_set_int(&int_value, payload))
			{
				ff2_voltimeter_set_bbp(int_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_AHRS_KP:
			ff2_debug_printf("Setting AHRS KP...\n");
			float_value = ahrs_kp;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				ff2_ahrs_set_kp(float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_AHRS_KI:
			ff2_debug_printf("Setting AHRS KI...\n");
			float_value = ahrs_ki;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				ff2_ahrs_set_ki(float_value);
				return true;
			}
			break;
		
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// ===============================================================================================
		// Roll Angular Speed PID
		// ===============================================================================================
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_ROLL_KP:
			ff2_debug_printf("Setting roll angular speed pid KP...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_ROLL];
			float_value = pid->KP;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KP = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_KP, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_ROLL_KI:
			ff2_debug_printf("Setting roll angular speed pid KI...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_ROLL];
			float_value = pid->KI;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KI = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_KI, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_ROLL_KD:
			ff2_debug_printf("Setting roll angular speed pid KD...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_ROLL];
			float_value = pid->KD;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KD = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_KD, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_ROLL_INTEGRAL_CONSTRAINT:
			ff2_debug_printf("Setting roll angular speed pid integral constraint...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_ROLL];
			float_value = pid->integral_constraint;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->integral_constraint = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_INTEGRAL_CONSTRAINT, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_ROLL_OUTPUT_CONSTRAINT:
			ff2_debug_printf("Setting roll angular speed pid output constraint...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_ROLL];
			float_value = pid->output_constraint;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->output_constraint = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_OUTPUT_CONSTRAINT, float_value);
				return true;
			}
			break;
		
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// ===============================================================================================
		// Pitch Angular Speed PID
		// ===============================================================================================
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_PITCH_KP:
			ff2_debug_printf("Setting pitch angular speed pid KP...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_PITCH];
			float_value = pid->KP;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KP = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_KP, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_PITCH_KI:
			ff2_debug_printf("Setting pitch angular speed pid KI...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_PITCH];
			float_value = pid->KI;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KI = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_KI, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_PITCH_KD:
			ff2_debug_printf("Setting pitch angular speed pid KD...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_PITCH];
			float_value = pid->KD;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KD = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_KD, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_PITCH_INTEGRAL_CONSTRAINT:
			ff2_debug_printf("Setting pitch angular speed pid integral constraint...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_PITCH];
			float_value = pid->integral_constraint;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->integral_constraint = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_INTEGRAL_CONSTRAINT, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_PITCH_OUTPUT_CONSTRAINT:
			ff2_debug_printf("Setting pitch angular speed pid output constraint...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_PITCH];
			float_value = pid->output_constraint;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->output_constraint = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_OUTPUT_CONSTRAINT, float_value);
				return true;
			}
			break;
		
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// ===============================================================================================
		// Yaw Angular Speed PID
		// ===============================================================================================
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_YAW_KP:
			ff2_debug_printf("Setting yaw angular speed pid KP...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_YAW];
			float_value = pid->KP;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KP = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_KP, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_YAW_KI:
			ff2_debug_printf("Setting yaw angular speed pid KI...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_YAW];
			float_value = pid->KI;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KI = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_KI, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_YAW_KD:
			ff2_debug_printf("Setting yaw angular speed pid KD...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_YAW];
			float_value = pid->KD;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KD = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_KD, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_YAW_INTEGRAL_CONSTRAINT:
			ff2_debug_printf("Setting yaw angular speed pid integral constraint...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_YAW];
			float_value = pid->integral_constraint;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->integral_constraint = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_INTEGRAL_CONSTRAINT, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGULAR_SPEED_YAW_OUTPUT_CONSTRAINT:
			ff2_debug_printf("Setting yaw angular speed pid output constraint...\n");
			pid = &ff2_pid_array[PID_ANGULAR_SPEED_YAW];
			float_value = pid->output_constraint;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->output_constraint = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_OUTPUT_CONSTRAINT, float_value);
				return true;
			}
			break;
		
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// ===============================================================================================
		// Roll Angle PID
		// ===============================================================================================
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		case BLUETOOTH_ID_PID_ANGLE_ROLL_KP:
			ff2_debug_printf("Setting roll angle pid KP...\n");
			pid = &ff2_pid_array[PID_ANGLE_ROLL];
			float_value = pid->KP;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KP = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_ROLL_KP, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_ROLL_KI:
			ff2_debug_printf("Setting roll angle pid KI...\n");
			pid = &ff2_pid_array[PID_ANGLE_ROLL];
			float_value = pid->KI;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KI = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_ROLL_KI, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_ROLL_KD:
			ff2_debug_printf("Setting roll angle pid KD...\n");
			pid = &ff2_pid_array[PID_ANGLE_ROLL];
			float_value = pid->KD;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KD = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_ROLL_KD, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_ROLL_BALANCE_FACTOR:
			ff2_debug_printf("Setting roll angle pid balance factor...\n");
			pid = &ff2_pid_array[PID_ANGLE_ROLL];
			float_value = pid->balance_factor;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->balance_factor = float_value;
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_ROLL_INTEGRAL_CONSTRAINT:
			ff2_debug_printf("Setting roll angle pid integral constraint...\n");
			pid = &ff2_pid_array[PID_ANGLE_ROLL];
			float_value = pid->integral_constraint;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->integral_constraint = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_ROLL_INTEGRAL_CONSTRAINT, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_ROLL_OUTPUT_CONSTRAINT:
			ff2_debug_printf("Setting roll angle pid output constraint...\n");
			pid = &ff2_pid_array[PID_ANGLE_ROLL];
			float_value = pid->output_constraint;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->output_constraint = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_ROLL_OUTPUT_CONSTRAINT, float_value);
				return true;
			}
			break;
		
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// ===============================================================================================
		// Pitch Angle PID
		// ===============================================================================================
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		case BLUETOOTH_ID_PID_ANGLE_PITCH_KP:
			ff2_debug_printf("Setting pitch angle pid KP...\n");
			pid = &ff2_pid_array[PID_ANGLE_PITCH];
			float_value = pid->KP;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KP = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_PITCH_KP, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_PITCH_KI:
			ff2_debug_printf("Setting pitch angle pid KI...\n");
			pid = &ff2_pid_array[PID_ANGLE_PITCH];
			float_value = pid->KI;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KI = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_PITCH_KI, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_PITCH_KD:
			ff2_debug_printf("Setting pitch angle pid KD...\n");
			pid = &ff2_pid_array[PID_ANGLE_PITCH];
			float_value = pid->KD;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KD = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_PITCH_KD, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_PITCH_BALANCE_FACTOR:
			ff2_debug_printf("Setting pitch angle pid balance factor...\n");
			pid = &ff2_pid_array[PID_ANGLE_PITCH];
			float_value = pid->balance_factor;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->balance_factor = float_value;
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_PITCH_INTEGRAL_CONSTRAINT:
			ff2_debug_printf("Setting pitch angle pid integral constraint...\n");
			pid = &ff2_pid_array[PID_ANGLE_PITCH];
			float_value = pid->integral_constraint;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->integral_constraint = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_PITCH_INTEGRAL_CONSTRAINT, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_PITCH_OUTPUT_CONSTRAINT:
			ff2_debug_printf("Setting pitch angle pid output constraint...\n");
			pid = &ff2_pid_array[PID_ANGLE_PITCH];
			float_value = pid->output_constraint;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->output_constraint = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_PITCH_OUTPUT_CONSTRAINT, float_value);
				return true;
			}
			break;

		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		// ===============================================================================================
		// Yaw Angle PID
		// ===============================================================================================
		// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		case BLUETOOTH_ID_PID_ANGLE_YAW_KP:
			ff2_debug_printf("Setting yaw angle pid KP...\n");
			pid = &ff2_pid_array[PID_ANGLE_YAW];
			float_value = pid->KP;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KP = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_YAW_KP, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_YAW_KI:
			ff2_debug_printf("Setting yaw angle pid KI...\n");
			pid = &ff2_pid_array[PID_ANGLE_YAW];
			float_value = pid->KI;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KI = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_YAW_KI, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_YAW_KD:
			ff2_debug_printf("Setting yaw angle pid KD...\n");
			pid = &ff2_pid_array[PID_ANGLE_YAW];
			float_value = pid->KD;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->KD = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_YAW_KD, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_YAW_BALANCE_FACTOR:
			ff2_debug_printf("Setting yaw angle pid balance factor...\n");
			pid = &ff2_pid_array[PID_ANGLE_YAW];
			float_value = pid->balance_factor;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->balance_factor = float_value;
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_YAW_INTEGRAL_CONSTRAINT:
			ff2_debug_printf("Setting yaw angle pid integral constraint...\n");
			pid = &ff2_pid_array[PID_ANGLE_YAW];
			float_value = pid->integral_constraint;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->integral_constraint = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_YAW_INTEGRAL_CONSTRAINT, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_PID_ANGLE_YAW_OUTPUT_CONSTRAINT:
			ff2_debug_printf("Setting yaw angle pid output constraint...\n");
			pid = &ff2_pid_array[PID_ANGLE_YAW];
			float_value = pid->output_constraint;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				pid->output_constraint = float_value;
				ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGLE_YAW_OUTPUT_CONSTRAINT, float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_LED_SCALE:
			ff2_debug_printf("Setting LED scale...\n");
			float_value = led_scale;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				ff2_led_set_scale(float_value);
				return true;
			}
			break;
		case BLUETOOTH_ID_BEHAVIOR_YAW_COMMAND_SCALE:
			ff2_debug_printf("Setting yaw command scale...\n");
			float_value = yaw_angle_command_scale;
			if (ff2_bluetooth_set_float(&float_value, payload))
			{
				yaw_angle_command_scale = float_value;
				return true;
			}
			break;
	}	

	ff2_debug_printf("Couldn't parse message!\n");

	return false;
}

bool ff2_bluetooth_set_float(float* value, char* payload)
{
	ff2_debug_printf("Current: %.02f\n", *value);
	
	if (sscanf(payload, "%f", value) == 1)
	{
		ff2_debug_printf("New: %.02f\n", *value);
		return true;
	}

	return false;
}

bool ff2_bluetooth_set_int(int* value, char* payload)
{
	ff2_debug_printf("Current: %i\n", *value);
	
	if (sscanf(payload, "%i", value) == 1)
	{
		ff2_debug_printf("New: %i\n", *value);
		return true;
	}

	return false;
}

