#include "compass.h"

void ff2_compass_initialize()
{
    ff2_delay(INITIALIZATION_TIME);
    ff2_debug_printf(">> Intitializing Compass...\n");

    int i;
    int xyz_total[3] = {0, 0, 0};
    bool error = false;

    ff2_delay(50);
    ff2_i2cd_write(COMPASS_ADDRESS, COMPASS_REGISTER_CONFA, 0x010 + COMPASS_POS_BIAS);   // Reg A DOR = 0x010 + MS1, MS0 set to pos bias
    // Note that the  very first measurement after a gain change maintains the same gain as the previous setting.
    // The new gain setting is effective from the second measurement and on.
    ff2_i2cd_write(COMPASS_ADDRESS, COMPASS_REGISTER_CONFB, 0x60); // Set the Gain to 2.5Ga (7:5->011)
    ff2_delay(100);
    ff2_compass_update();

    for (i = 0; i < 10; i++) // Collect 10 samples
    {  
        ff2_i2cd_write(COMPASS_ADDRESS, COMPASS_REGISTER_MODE, 1);
        ff2_delay(80);
        ff2_compass_update();       // Get the raw values in case the scales have already been changed.

        // Since the measurements are noisy, they should be averaged rather than taking the max.
        xyz_total[X] += ff2_compass_data.x;
        xyz_total[Y] += ff2_compass_data.y;
        xyz_total[Z] += ff2_compass_data.z;

        // Detect saturation.
        if (-4096 >= FF2_MIN(ff2_compass_data.x, FF2_MIN(ff2_compass_data.y, ff2_compass_data.z))) 
        {
            error = true;
            break;              // Breaks out of the for loop.  No sense in continuing if we saturated.
        }
    }

    // Apply the negative bias. (Same gain)
    ff2_i2cd_write(COMPASS_ADDRESS, COMPASS_REGISTER_CONFA, 0x010 + COMPASS_NEG_BIAS);   // Reg A DOR = 0x010 + MS1, MS0 set to negative bias.
    for (i = 0; i < 10; i++) 
    {
        ff2_i2cd_write(COMPASS_ADDRESS, COMPASS_REGISTER_MODE, 1);
        ff2_delay(80);
        ff2_compass_update();               // Get the raw values in case the scales have already been changed.

        // Since the measurements are noisy, they should be averaged.
        xyz_total[X] -= ff2_compass_data.x;
        xyz_total[Y] -= ff2_compass_data.y;
        xyz_total[Z] -= ff2_compass_data.z;

        // Detect saturation.
        if (-4096 >= FF2_MIN(ff2_compass_data.x, FF2_MIN(ff2_compass_data.y, ff2_compass_data.z))) 
        {
            error = true;
            break;              // Breaks out of the for loop.  No sense in continuing if we saturated.
        }
    }

    compass_gain[X] = fabsf(660.0f * COMPASS_X_SELF_TEST_GAUSS * 2.0f * 10.0f / xyz_total[X]);
    compass_gain[Y] = fabsf(660.0f * COMPASS_Y_SELF_TEST_GAUSS * 2.0f * 10.0f / xyz_total[Y]);
    compass_gain[Z] = fabsf(660.0f * COMPASS_Z_SELF_TEST_GAUSS * 2.0f * 10.0f / xyz_total[Z]);

    // leave test mode
    ff2_i2cd_write(COMPASS_ADDRESS, COMPASS_REGISTER_CONFA, 0x70);   // Configuration Register A  -- 0 11 100 00  num samples: 8 ; output rate: 15Hz ; normal measurement mode
    ff2_i2cd_write(COMPASS_ADDRESS, COMPASS_REGISTER_CONFB, 0x20);   // Configuration Register B  -- 001 00000    configuration gain 1.3Ga
    ff2_i2cd_write(COMPASS_ADDRESS, COMPASS_REGISTER_MODE, 0x00);    // Mode register             -- 000000 00    continuous Conversion Mode
    ff2_delay(100);

    if (error) 
    {                // Something went wrong so get a best guess
        compass_gain[X] = 1.0f;
        compass_gain[Y] = 1.0f;
        compass_gain[Z] = 1.0f;
    }

    ff2_debug_printf(">> Callibrating Compass...\n");
    ff2_debug_printf("Move the multi in all directions. You have 30 seconds.\n");

    float current_time = ff2_time();
    ff2_vector3f min;
    ff2_vector3f max;

    ff2_compass_update();

    min.x = ff2_compass_data.x;
    min.y = ff2_compass_data.y;
    min.z = ff2_compass_data.z;

    max.x = ff2_compass_data.x;
    max.y = ff2_compass_data.y;
    max.z = ff2_compass_data.z;

    float x_callibration[COMPASS_CALLIBRATION_SIZE];
    float y_callibration[COMPASS_CALLIBRATION_SIZE];
    float z_callibration[COMPASS_CALLIBRATION_SIZE];

    float x_filtered[COMPASS_CALLIBRATION_SIZE];
    float y_filtered[COMPASS_CALLIBRATION_SIZE];
    float z_filtered[COMPASS_CALLIBRATION_SIZE];    

    ff2_led_set_blink(LED_BLUE, 0.2f, 0.2f);
    for (int i = 0; i < COMPASS_CALLIBRATION_SIZE; i++)
    {
        ff2_delay(80); //15 Hz
        ff2_compass_update();
        ff2_led_update();
        x_callibration[i] = ff2_compass_data.x;
        y_callibration[i] = ff2_compass_data.y;
        z_callibration[i] = ff2_compass_data.z;
        ff2_printf("%f %f %f\n", ff2_compass_data.x, ff2_compass_data.y, ff2_compass_data.z);
    }
    ff2_led_set_fixed(LED_BLUE, 0);

    ff2_apply_median_filter_7(x_callibration, x_filtered, COMPASS_CALLIBRATION_SIZE);
    ff2_apply_median_filter_7(y_callibration, y_filtered, COMPASS_CALLIBRATION_SIZE);
    ff2_apply_median_filter_7(z_callibration, z_filtered, COMPASS_CALLIBRATION_SIZE);

    for (int i = 0; i < COMPASS_CALLIBRATION_SIZE; i++)
    {
        min.x = FF2_MIN(min.x, x_filtered[i]);
        min.y = FF2_MIN(min.y, y_filtered[i]);
        min.z = FF2_MIN(min.z, z_filtered[i]);

        max.x = FF2_MAX(max.x, x_filtered[i]);
        max.y = FF2_MAX(max.y, y_filtered[i]);
        max.z = FF2_MAX(max.z, z_filtered[i]);
    }

    compass_offset[X] = (min.x + max.x) / 2.0f;
    compass_offset[Y] = (min.y + max.y) / 2.0f;
    compass_offset[Z] = (min.z + max.z) / 2.0f;

    ff2_debug_printf(">> Compass callibrated: x: %f\ny: %f\nz: %f\n", compass_offset[X], compass_offset[Y], compass_offset[Z]);
}

void ff2_compass_update()
{
    ff2_i2cd_read(COMPASS_ADDRESS, COMPASS_DATA_REGISTER, 6, i2cd_output_6x);
    
    // During calibration, compass_gain is 1.0, so the read returns normal non-calibrated values.
    // After calibration is done, compass_gain is set to calculated gain values.
    ff2_compass_data.x = (int16_t) (i2cd_output_6x[0] << 8 | i2cd_output_6x[1]) * compass_gain[X] - compass_offset[X];
    ff2_compass_data.z = (int16_t) (i2cd_output_6x[2] << 8 | i2cd_output_6x[3]) * compass_gain[Z] - compass_offset[Z];
    ff2_compass_data.y = (int16_t) (i2cd_output_6x[4] << 8 | i2cd_output_6x[5]) * compass_gain[Y] - compass_offset[Y];    
}

void ff2_compass_debug()
{
    ff2_debug_printf(">> Starting compass debug...\n");
    ff2_debug_printf("Compass offsets - x: %f y: %f z: %f\n", compass_offset[X], compass_offset[Y], compass_offset[Z]);
    ff2_debug_printf("Compass gains - x: %f y: %f z: %f\n", compass_gain[X], compass_gain[Y], compass_gain[Z]);
    
    // int data_counter = 0;
    // float current_time = ff2_time();

    // led_set_blink(LED_RED, 0.2f, 0.2f);
    // while (ff2_time() - current_time < 90.0f)
    // {
    //     ff2_delay(66); //15 Hz
    //     ff2_compass_update();
    //     led_update();
    //     compass_values[data_counter] = ff2_compass_data.x;
    //     compass_values[data_counter + 1] = ff2_compass_data.y;
    //     compass_values[data_counter + 2] = ff2_compass_data.z;        
    //     data_counter += 3;
    // }
    // led_set_fixed(LED_BLUE, 0);

    // for (unsigned i = 0; i < data_counter; i += 3)
    // {
    //     ff2_printf("%f %f %f\n", compass_values[i], compass_values[i + 1], compass_values[i + 2]);
    // }

    // while(true);
}