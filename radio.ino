#include "radio.h"

void ff2_radio_initialize()
{
	ff2_delay(INITIALIZATION_TIME);
	ff2_debug_printf(">> Initializing Radio...\n");

	radio_ports[0] = RADIO_CHAN_1;
	radio_ports[1] = RADIO_CHAN_2;
	radio_ports[2] = RADIO_CHAN_3;
	radio_ports[3] = RADIO_CHAN_4;
	radio_ports[4] = RADIO_CHAN_5;
	radio_ports[5] = RADIO_CHAN_6;

	radio_min[0] = 1114;
	radio_max[0] = 1870;
	radio_min[1] = 1170;
	radio_max[1] = 1774;
	radio_min[2] = 1130;
	radio_max[2] = 1772;
	radio_min[3] = 1130;
	radio_max[3] = 1830;
	radio_min[4] = 995;
	radio_max[4] = 2006;
	radio_min[5] = 995;
	radio_max[5] = 2006;

	for (byte i = 0; i < RADIO_CHAN_COUNT; i++)
	{
		radio_timers[i] = 0;
		radio_widths[i] = 0;
	}

	attachInterrupt(radio_ports[0], ff2_interrupt_0, CHANGE);
	attachInterrupt(radio_ports[1], ff2_interrupt_1, CHANGE);
	attachInterrupt(radio_ports[2], ff2_interrupt_2, CHANGE);
	attachInterrupt(radio_ports[3], ff2_interrupt_3, CHANGE);
	attachInterrupt(radio_ports[4], ff2_interrupt_4, CHANGE);
	attachInterrupt(radio_ports[5], ff2_interrupt_5, CHANGE);

	// Talvez criar uma calibracao pra isso
	radio_mapping[AILERON] 		= 0;
	radio_mapping[ELEVATOR] 	= 1;
	radio_mapping[RUDDER]		= 3;
	radio_mapping[THROTTLE] 	= 2;
	radio_mapping[DIAL_LEFT] 	= 5;
	radio_mapping[DIAL_RIGHT]	= 4;
}

float ff2_radio_chan_value(byte channel)
{
	byte index = radio_mapping[channel];
	float value;
	
	if (channel == DIAL_RIGHT || channel == DIAL_LEFT || channel == THROTTLE)
	{
		value = ff2_range_mapf(radio_widths[index], radio_min[index], radio_max[index], 0.0f, 1.0f);
		value = ff2_apply_deadbandf(value, 0.01f);
		value = FF2_MIN(FF2_MAX(value, 0.0f), 1.0f);
	}
	else
	{
		value = ff2_range_mapf(radio_widths[index], radio_min[index], radio_max[index], -1.0f, 1.0f);
		value = ff2_apply_deadbandf(value, 0.08f);
		value = FF2_MIN(FF2_MAX(value, -1.0f), 1.0f);
	}

	return value;
}

void ff2_interrupt(byte channel) {
	if (digitalRead(radio_ports[channel]) == HIGH) {
		radio_timers[channel] = ff2_time_micros();
	} else {
		radio_widths[channel] = ff2_time_micros() - radio_timers[channel];
	}
}

void ff2_radio_debug()
{
	ff2_printf("THR:%f AIL:%f ELE:%f RUD:%f DL:%f DR:%f\n", ff2_radio_chan_value(THROTTLE), ff2_radio_chan_value(AILERON), ff2_radio_chan_value(ELEVATOR), ff2_radio_chan_value(RUDDER), ff2_radio_chan_value(DIAL_LEFT), ff2_radio_chan_value(DIAL_RIGHT));
}

void ff2_interrupt_0()
{
	ff2_interrupt(0);	
}

void ff2_interrupt_1()
{
	ff2_interrupt(1);	
}

void ff2_interrupt_2()
{
	ff2_interrupt(2);	
}

void ff2_interrupt_3()
{
	ff2_interrupt(3);	
}

void ff2_interrupt_4()
{
	ff2_interrupt(4);	
}

void ff2_interrupt_5()
{
	ff2_interrupt(5);	
}