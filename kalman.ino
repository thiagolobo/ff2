#include "kalman.h"
/*


	Link is http://part.put.poznan.pl/articles/article.pdf



*/
	
/*
	This function has several restrictions:

	- measurement is a 2x1 array containing the height and acceleration values
		provided by the sensors, IN THIS ORDER.

	- last_estimate is an 3x1 array containing previsions regarding the model
		defined within the function, and contains values for the height, 
		acceleration, and velocity. IN THIS ORDER.

	- covar is a covariance matrix using the 3 estimated variables,
		height, acceleration and velocity.IN THIS ORDER.
		
*/

// float* ff2_kalman_filter (float* measurement, float* last_estimate, float* Covar)
// {
// 	float det;
// 	// v_k = v_k-1 + a_k-1*T
// 	// h_k = h_k-1 + v_k-1*T + a_k-1*T^2/2

// 	last_estimate[0] = last_estimate[0] + T*T/2.0f*last_estimate[1] + T*last_estimate[2];

// 	last_estimate[2] = T*last_estimate[1] + last_estimate[2];

// 	// X = [x a v]'


// 	// C = 	|1	0	0|
// 	//		|0	1	0|

// 	//K(k) = P(k|k − 1)CT(CP(k|k − 1)CT + R)_−1

// 	// matrix P = |xx	xa	xv|	0
// 	//			  |ax 	aa 	av|	1
// 	//			  |vx	va	vv|	2
// 	//				0	1	2

// 	// R = 	|r00 r01|
// 	// 		|r10 r11|

// 		// K = 
// 		// / xx (aa + r11)   xa (ax + r10)  xa (r00 + xx)   xx (r01 + xa) \
// 		// | ------------- - -------------, ------------- - ------------- |
// 		// |       #1              #1             #1              #1      |
// 		// |                                                              |
// 		// | ax (aa + r11)   aa (ax + r10)  aa (r00 + xx)   ax (r01 + xa) |
// 		// | ------------- - -------------, ------------- - ------------- |
// 		// |       #1              #1             #1              #1      |
// 		// |                                                              |
// 		// | vx (aa + r11)   va (ax + r10)  va (r00 + xx)   vx (r01 + xa) |
// 		// | ------------- - -------------, ------------- - ------------- |
// 		// \       #1              #1             #1              #1      /

// 		// where

// 		//    #1 == aa r00 - ax r01 	+ 
// 		// 			aa xx - ax xa 		+ 
// 		// 			r00 r11 - r01 r10 	+ 
// 		// 			r11 xx - r10 xa

// 	// Updates

// 	float  NSEI  	= Covar[1*3 + 1] + R[1*2 + 1]; 		// aa + r11
// 	float  NAME 	= Covar[1*3 + 0] + R[1*2 + 0];		// ax + r10

// 	float  BLA 	= R[0*2 + 0]	 + Covar[0*3 + 0];	// r00 + xx
// 	float  BLE 	= R[0*2 + 1]	 + Covar[0*3 + 1];	// r01 + xa

// 	det = 	Covar[0*3 + 0]*R[0*2 + 0] 		- Covar[1*3 + 0]*R[0*2 + 1] 		+	
// 			Covar[1*3 + 1]*Covar[0*3+0] 	- Covar[1*3 + 0]*Covar[0*3 + 1] 	+ // pode so elevar ao quadrado
// 			R[0*2 + 0]*R[1*2 + 1]			- R[0*2 + 1]*R[1*2 + 0]				+
// 			R[1*2 + 1]*Covar[0*3+0]			- Covar[0*3 + 1]					;


	
//  	float* K = malloc(3*2*sizeof(float));

//  	K[0*3+0] =	(Covar[0*3 + 0]*(NSEI) 	- Covar[0*3 + 1]*(NAME))/det;		//K11		
// 	K[0*3+1] =	(Covar[0*3 + 1]*(BLA) 	- Covar[0*3 + 0]*(BLE))/det;		//K12

// 	K[1*3+0] =	(Covar[1*3 + 0]*(NSEI)	- Covar[1*3 + 1]*(NAME))/det;		//K21
// 	K[1*3+1] =	(Covar[1*3 + 1]*(BLA) 	- Covar[1*3 + 0]*(BLE))/det;		//K22

// 	K[2*3+0] =	(Covar[2*3 + 0]*(NSEI) 	- Covar[2*3 + 1]*(NAME))/det;		//K31
// 	K[2*3+1] =	(Covar[2*3 + 1]*(BLA) 	- Covar[2*3 + 0]*(BLE))/det;		//K32

// 	float* estimate_aux = malloc(3*sizeof(float));

// 	estimate_aux[0] = K[0*3+0]*(measurement[1] - last_estimate[1]) + K[1*3+1]*(measurement[0] - last_estimate[0]);
// 	estimate_aux[1] = K[0*3+0]*(measurement[1] - last_estimate[1]) + K[1*3+1]*(measurement[0] - last_estimate[0]);
// 	estimate_aux[2] = K[0*3+0]*(measurement[1] - last_estimate[1]) + K[1*3+1]*(measurement[0] - last_estimate[0]);

// 	last_estimate[0] = estimate_aux[0];	//
// 	last_estimate[1] = estimate_aux[1];	//	Eu acho que [e so mudar o endereco do primeiro cara ne ? 
// 	last_estimate[2] = estimate_aux[2];	// 


// // PNew =
 
// // [ - P00*(K00 - 1) - K01*P10, - P01*(K00 - 1) - K01*P11, - P02*(K00 - 1) - K01*P12]
// // [ - P10*(K11 - 1) - K10*P00, - P11*(K11 - 1) - K10*P01, - P12*(K11 - 1) - K10*P02]
// // [   P20 - K20*P00 - K21*P10,   P21 - K20*P01 - K21*P11,   P22 - K20*P02 - K21*P12]

// 	float* Covar_aux = malloc(3*3*sizeof(float));

// 	Covar_aux[0*3+0] = - Covar[0*3+0]*(K[0*2+0] - 1) - K[0*2+1]*Covar[1*3+0];
// 	Covar_aux[0*3+1] = - Covar[0*3+1]*(K[0*2+0] - 1) - K[0*2+1]*Covar[1*3+1];
// 	Covar_aux[0*3+2] = - Covar[0*3+2]*(K[0*2+0] - 1) - K[0*2+1]*Covar[1*3+2];

// 	Covar_aux[1*3+0] = - Covar[1*3+0]*(K11 - 1) - K[1*2+0]*Covar[0*3+0];
// 	Covar_aux[1*3+1] = - Covar[1*3+1]*(K11 - 1) - K[1*2+0]*Covar[0*3+1];
// 	Covar_aux[1*3+2] = - Covar[1*3+2]*(K11 - 1) - K[1*2+0]*Covar[0*3+2];

// 	Covar_aux[2*3+0] = Covar[2*3+0] - K[2*2+0]*Covar[0*3+0] - K[2*2+1]*Covar[1*3+0];
// 	Covar_aux[2*3+1] = Covar[2*3+1] - K[2*2+0]*Covar[0*3+1] - K[2*2+1]*Covar[1*3+1];
// 	Covar_aux[2*3+2] = Covar[2*3+2] - K[2*2+0]*Covar[0*3+2] - K[2*2+1]*Covar[1*3+2];


// 	Covar[0*3+0] = Covar_aux[0*3+0];
// 	Covar[0*3+1] = Covar_aux[0*3+1];
// 	Covar[0*3+2] = Covar_aux[0*3+2];

// 	Covar[1*3+0] = Covar_aux[1*3+0];
// 	Covar[1*3+1] = Covar_aux[1*3+1];
// 	Covar[1*3+2] = Covar_aux[1*3+2];

// 	Covar[2*3+0] = Covar_aux[2*3+0];
// 	Covar[2*3+1] = Covar_aux[2*3+1];
// 	Covar[2*3+2] = Covar_aux[2*3+2];

// 	free(estimate_aux);
// 	free(Covar_aux);
// 	return last_estimate;

// }