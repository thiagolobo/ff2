#include "blackbox.h"

void ff2_blackbox_initialize()
{
#ifdef BLACKBOX_ENABLED
	ff2_delay(INITIALIZATION_TIME);
	ff2_debug_printf(">> Initializing Blackbox...\n");
	
	if (persistent_has_data)
	{	
		// Flash Offset	
		blackbox_flash_offset = ff2_persistent_read_int(PERSISTENT_OFFSET_BLACKBOX_OFFSET);
		// Flight ID
		ff2_flight_id = ff2_persistent_read_int(PERSISTENT_OFFSET_BLACKBOX_FLIGHT_ID);
		ff2_persistent_write_int(PERSISTENT_OFFSET_BLACKBOX_FLIGHT_ID, ++ff2_flight_id);
	}
	else
	{
		ff2_persistent_write_int(PERSISTENT_OFFSET_BLACKBOX_OFFSET, blackbox_flash_offset);
		ff2_persistent_write_int(PERSISTENT_OFFSET_BLACKBOX_FLIGHT_ID, ff2_flight_id);
	}
#endif
}

void ff2_blackbox_write_float(float value, int id)
{
#ifdef BLACKBOX_ENABLED	
	blackbox_buffer[blackbox_write_pointer++ % BLACKBOX_BUFFER_SIZE] = id;
	memcpy(&blackbox_buffer[blackbox_write_pointer % BLACKBOX_BUFFER_SIZE], &value, sizeof(float));
	blackbox_write_pointer += sizeof(float);
	blackbox_last_time = ff2_time();
#endif
}

void ff2_blackbox_write_int(int value, int id)
{
#ifdef BLACKBOX_ENABLED
	blackbox_buffer[blackbox_write_pointer++ % BLACKBOX_BUFFER_SIZE] = id;
	memcpy(&blackbox_buffer[blackbox_write_pointer % BLACKBOX_BUFFER_SIZE], &value, sizeof(int));
	blackbox_write_pointer += sizeof(int);
	blackbox_last_time = ff2_time();
#endif
}

void ff2_blackbox_dump()
{
#ifdef BLACKBOX_ENABLED
	// Check for flash memory overflow
	if (blackbox_flash_offset + BLACKBOX_BUFFER_SIZE + 2 * sizeof(int) + sizeof(float) >= FLASH_SIZE)
	{
		// Reset offset pointer
		blackbox_flash_offset = ff2_persistent_length();
	}

	ff2_flash_write(blackbox_flash_offset, blackbox_buffer, BLACKBOX_BUFFER_SIZE);
	
	// Padding:
	// Write pointer (know if there was buffer overflow)
	byte bytes[sizeof(int)];
	memcpy(bytes, &blackbox_write_pointer, sizeof(int));
	ff2_flash_write(blackbox_flash_offset + BLACKBOX_BUFFER_SIZE, bytes, sizeof(int));
	
	// Timestamp of last write (plot reference)
	memcpy(bytes, &blackbox_last_time, sizeof(float));
	ff2_flash_write(blackbox_flash_offset + BLACKBOX_BUFFER_SIZE + sizeof(int), bytes, sizeof(float));
	
	// Flight Identifier
	memcpy(bytes, &ff2_flight_id, sizeof(int));
	ff2_flash_write(blackbox_flash_offset + BLACKBOX_BUFFER_SIZE + sizeof(int) + sizeof(float), bytes, sizeof(float));

	// Update offset
	blackbox_flash_offset += BLACKBOX_BUFFER_SIZE + 2 * sizeof(int) + sizeof(float);
	ff2_persistent_write_int(PERSISTENT_OFFSET_BLACKBOX_OFFSET, blackbox_flash_offset);

	blackbox_write_pointer = 0;
#endif
}
