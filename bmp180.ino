#include "bmp180.h"

void ff2_bmp180_initialize()
{
	ff2_delay(INITIALIZATION_TIME);
	ff2_debug_printf(">> Initializing BMP180...\n");

	ff2_moving_average_initialize(&ff2_altitude, 10);

	oversampling = BMP180_SAMPLING_RATE;

	if (oversampling == BMP180_ULTRALOWPOWER)
	{
		raw_pressure_wait_time = 0.005f;
	}
	else if (oversampling == BMP180_STANDARD) 
	{
		raw_pressure_wait_time = 0.008f;
	}
	else if (oversampling == BMP180_HIGHRES) 
	{
		raw_pressure_wait_time = 0.014f;
	}
	else 
	{
		raw_pressure_wait_time = 0.026f;
	}

	int bmp180_raw_callibration_parameters[BMP180_RAW_CALLIBRATION_SIZE];

	ff2_i2cd_read(BMP180_ADDRESS, BMP180_CAL_AC1, BMP180_RAW_CALLIBRATION_SIZE, bmp180_raw_callibration_parameters);

	ac1 = (int16_t) ((bmp180_raw_callibration_parameters[0]  << 8) | bmp180_raw_callibration_parameters[1]);
	ac2 = (int16_t) ((bmp180_raw_callibration_parameters[2]  << 8) | bmp180_raw_callibration_parameters[3]);
	ac3 = (int16_t) ((bmp180_raw_callibration_parameters[4]  << 8) | bmp180_raw_callibration_parameters[5]);
	ac4 = (int16_t) ((bmp180_raw_callibration_parameters[6]  << 8) | bmp180_raw_callibration_parameters[7]);
	ac5 = (int16_t) ((bmp180_raw_callibration_parameters[8]  << 8) | bmp180_raw_callibration_parameters[9]);
	ac6 = (int16_t) ((bmp180_raw_callibration_parameters[10] << 8) | bmp180_raw_callibration_parameters[11]);
	b1 =  (int16_t) ((bmp180_raw_callibration_parameters[12] << 8) | bmp180_raw_callibration_parameters[13]);
	b2 =  (int16_t) ((bmp180_raw_callibration_parameters[14] << 8) | bmp180_raw_callibration_parameters[15]);
	mb =  (int16_t) ((bmp180_raw_callibration_parameters[16] << 8) | bmp180_raw_callibration_parameters[17]);
	mc =  (int16_t) ((bmp180_raw_callibration_parameters[18] << 8) | bmp180_raw_callibration_parameters[19]);
	md =  (int16_t) ((bmp180_raw_callibration_parameters[20] << 8) | bmp180_raw_callibration_parameters[21]);
}

void ff2_bmp180_read_raw_temperature()
{
	ff2_i2cd_write(BMP180_ADDRESS, BMP180_CONTROL, BMP180_READTEMPCMD);
	last_read_time = ff2_time();
}

void ff2_bmp180_read_raw_pressure()
{
	ff2_i2cd_write(BMP180_ADDRESS, BMP180_CONTROL, BMP180_READPRESSURECMD + (oversampling << 6));
	last_read_time = ff2_time();
}

int32_t ff2_bmp180_compute_B5()
{
	int32_t X1 = (raw_temperature - (int32_t) ac6) * ((int32_t) ac5) >> 15;
	int32_t X2 = ((int32_t) mc << 11) / (X1 + (int32_t) md);
	return X1 + X2;
}

void ff2_bmp180_update()
{	
	switch (step)
	{
		case 0:
			ff2_bmp180_read_raw_temperature();
			++step;
			break;
		case 1:
			if (ff2_time() - last_read_time >= 0.005f)
			{
				ff2_i2cd_read(BMP180_ADDRESS, BMP180_TEMPDATA, 2, auxiliary_array);
				raw_temperature = (uint16_t) ((auxiliary_array[0] << 8) | auxiliary_array[1]);
				ff2_bmp180_read_raw_pressure();
				++step;
			}
			break;
		case 2:
			if (ff2_time() - last_read_time >= raw_pressure_wait_time)
			{
				int32_t B3, B5, B6, X1, X2, X3, p;
  				uint32_t B4, B7;

  				ff2_i2cd_read(BMP180_ADDRESS, BMP180_PRESSUREDATA, 2, auxiliary_array);
				raw_pressure = (uint16_t) ((auxiliary_array[0] << 8) | auxiliary_array[1]);
				raw_pressure <<= 8;
				int value;
				ff2_i2cd_read(BMP180_ADDRESS, BMP180_PRESSUREDATA + 2, &value);
				raw_pressure |= (uint8_t) value;
				raw_pressure >>= (8 - oversampling);
				
				// ----- calculate pressure & temperature -----

				B5 = ff2_bmp180_compute_B5();				
				B6 = B5 - 4000;
				X1 = ((int32_t) b2 * ((B6 * B6) >> 12)) >> 11;
				X2 = ((int32_t) ac2 * B6) >> 11;
				X3 = X1 + X2;
				B3 = ((((int32_t) ac1 * 4 + X3) << oversampling) + 2) / 4;

				X1 = ((int32_t) ac3 * B6) >> 13;
				X2 = ((int32_t) b1 * ((B6 * B6) >> 12)) >> 16;
				X3 = ((X1 + X2) + 2) >> 2;
				B4 = ((uint32_t) ac4 * (uint32_t)(X3 + 32768)) >> 15;
				B7 = ((uint32_t) raw_pressure - B3) * (uint32_t)(50000UL >> oversampling);
				
				if (B7 < 0x80000000) {
					p = (B7 * 2) / B4;
				} else {
					p = (B7 / B4) * 2;
				}
				X1 = (p >> 8) * (p >> 8);
				X1 = (X1 * 3038) >> 16;
				X2 = (-7357 * p) >> 16;

				p = p + ((X1 + X2 + (int32_t) 3791) >> 4);

				ff2_pressure = p;

				ff2_moving_average_push(&ff2_altitude, 44330.0f * (1.0f - FF2_POW(ff2_pressure / (100.0f * 1013.25f), 1.0f / 5.255f)));

				ff2_temperature = (B5 + 8) >> 4;
  				ff2_temperature /= 10;
  				
				step = 0;				
			}
			break;
	}
}