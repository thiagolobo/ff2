#pragma once

#include "arduino_wrapper.h"

#include "i2c_device.h"
#include "flash.h"
#include "radio.h"
#include "led.h"
#include "motor.h"
#include "persistent.h"
#include "bluetooth.h"
#include "voltimeter.h"
#include "moving_average.h"
#include "mpu6050.h"
#include "compass.h"
#include "gps.h"
#include "ahrs.h"
#include "pid_controller.h"
#include "bmp180.h"

#define BEHAVIOR_UPDATE_RATE 200 //Hz

#define BEHAVIOR_STATE_INITIALIZATION 	0
#define BEHAVIOR_STATE_SAFE				1
#define BEHAVIOR_STATE_ARMED 			2

#define BEHAVIOR_FLIGHT_LEVEL			0	
#define BEHAVIOR_FLIGHT_ACCRO			1
#define BEHAVIOR_FLIGHT_HORIZON			2
#define BEHAVIOR_FLIGHT_POSITION_HOLD	3
#define BEHAVIOR_FLIGHT_ALTITUDE_HOLD	4
#define BEHAVIOR_FLIGHT_AUTONAV			5

float roll_pitch_angle_command_scale = 20.0f;
float yaw_angle_command_scale = 0.4f;

unsigned long ff2_synchronizer;
byte ff2_behavior_state = BEHAVIOR_STATE_INITIALIZATION;
float behavior_last_time = 0.0f;
float behavior_current_time;
float behavior_accumulator = 0.0f;
byte behavior_flight_mode = BEHAVIOR_FLIGHT_LEVEL;

float yaw_angle_setpoint = 0.0f;

void ff2_behavior_initialize();
void ff2_behavior_update();
void ff2_behavior_self_check();
void ff2_behavior_wait_and_arm();
