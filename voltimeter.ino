#include "voltimeter.h"

void ff2_voltimeter_initialize()
{
	ff2_delay(INITIALIZATION_TIME);
	ff2_debug_printf(">> Initializing Voltimeter...\n");

	if (persistent_has_data)
	{	
		voltimeter_threshold = ff2_persistent_read_float(PERSISTENT_OFFSET_VOLTIMETER_THRESHOLD);
		voltimeter_bbp = ff2_persistent_read_int(PERSISTENT_OFFSET_VOLTIMETER_BBP);
	}
	else
	{
		ff2_persistent_write_float(PERSISTENT_OFFSET_VOLTIMETER_THRESHOLD, voltimeter_threshold);
		ff2_persistent_write_int(PERSISTENT_OFFSET_VOLTIMETER_BBP, voltimeter_bbp);
	}

	ff2_moving_average_initialize(&voltimeter_average, VOLTIMETER_AVERAGE_SIZE);
}

void ff2_voltimeter_update()
{
	ff2_moving_average_push(&voltimeter_average, ff2_read_volts(VOLTIMETER_PORT) * VOLTIMETER_VOLTAGE_DIVIDER_INVERSE_GAIN * VOLTIMETER_CORRECTION_FACTOR);
	float voltimeter_voltage = voltimeter_average.average;

	if (voltimeter_voltage < voltimeter_threshold)
	{
		ff2_led_set_fixed(LED_RED, 255);
	}
	else
	{
		ff2_led_set_fixed(LED_RED, 0);
	}

	if (ff2_synchronizer % voltimeter_bbp == 0)
	{
		ff2_blackbox_write_float(voltimeter_voltage, BLACKBOX_ID_VOLTIMETER);
		ff2_debug_printf("%f volts.\n", voltimeter_voltage);
	}
}

void ff2_voltimeter_set_threshold(float value)
{
	voltimeter_threshold = value;
	ff2_persistent_write_float(PERSISTENT_OFFSET_VOLTIMETER_THRESHOLD, voltimeter_threshold);
}

void ff2_voltimeter_set_bbp(int value)
{
	voltimeter_bbp = value;
	ff2_persistent_write_int(PERSISTENT_OFFSET_VOLTIMETER_BBP, voltimeter_bbp);
}

void ff2_voltimeter_debug()
{
}
