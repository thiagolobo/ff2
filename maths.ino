#include "maths.h"

int ff2_constrain(int amount, int low, int high)
{
	return (amount > high) ? high : ((amount < low) ? low : amount);
}

float ff2_constrainf(float amount, float low, float high)
{
	return (amount > high) ? high : ((amount < low) ? low : amount);
}

int ff2_apply_deadband(int value, int deadband)
{
	if (FF2_ABS(value) < deadband) {
        value = 0;
    } else if (value > 0) {
        value -= deadband;
    } else if (value < 0) {
        value += deadband;
    }

    return value;
}

float ff2_apply_deadbandf(float value, float deadband)
{
	if (FF2_ABS(value) < deadband) {
        value = 0;
    } else if (value > 0) {
        value -= deadband;
    } else if (value < 0) {
        value += deadband;
    }
    
    return value;
}

int ff2_range_map(int x, int srcMin, int srcMax, int destMin, int destMax)
{
    return round(ff2_range_mapf(x, srcMin, srcMax, destMin, destMax));
}

float ff2_range_mapf(float x, float srcMin, float srcMax, float destMin, float destMax)
{
    return (x - srcMin) / (srcMax - srcMin) * (destMax - destMin) + destMin;
}


float ff2_sin(float x)
{
    #ifdef FF2_FAST_TRIGONOMETRY
        #define sinPolyCoef3 -1.666665710e-1f
        #define sinPolyCoef5  8.333017292e-3f
        #define sinPolyCoef7 -1.980661520e-4f
        #define sinPolyCoef9  2.600054768e-6f
        int xint = x;
        
        if (xint < -32 || xint > 32) 
        {
            return 0.0f;
        }
        
        while (x > FF2_PI) 
        {
            x -= (FF2_TAU);
        }
        
        while (x < -FF2_PI)
        {
            x += (FF2_TAU);
        }
        
        if (x > (0.5f * FF2_PI)) 
        {
            x = (0.5f * FF2_PI) - (x - (0.5f * FF2_PI));
        }
        else if (x < -(0.5f * FF2_PI)) 
        {
            x = -(0.5f * FF2_PI) - ((0.5f * FF2_PI) + x);
        }
       
        float x2 = FF2_SQR(x);
        return x + x * x2 * (sinPolyCoef3 + x2 * (sinPolyCoef5 + x2 * (sinPolyCoef7 + x2 * sinPolyCoef9)));
    #else
        return sin(x);
    #endif
}

float ff2_cos(float x)
{
    #ifdef FF2_FAST_TRIGONOMETRY
        return ff2_sin(x + (0.5f * FF2_PI));
    #else
        return cos(x);
    #endif
}

float ff2_tan(float x)
{
    #ifdef FF2_FAST_TRIGONOMETRY
        return ff2_sin(x) / ff2_cos(x);
    #else
        return tan(x);
    #endif
}

float ff2_atan2(float y, float x)
{
    #ifdef FF2_FAST_TRIGONOMETRY
        #define atanPolyCoef1  3.14551665884836e-07f
        #define atanPolyCoef2  0.99997356613987f
        #define atanPolyCoef3  0.14744007058297684f
        #define atanPolyCoef4  0.3099814292351353f
        #define atanPolyCoef5  0.05030176425872175f
        #define atanPolyCoef6  0.1471039133652469f
        #define atanPolyCoef7  0.6444640676891548f

        float res, absX, absY;
        absX = fabsf(x);
        absY = fabsf(y);
        res  = FF2_MAX(absX, absY);
        
        if (res) 
        {
            res = FF2_MIN(absX, absY) / res;
        }
        else 
        {
            res = 0.0f;
        }

        res = -((((atanPolyCoef5 * res - atanPolyCoef4) * res - atanPolyCoef3) * res - atanPolyCoef2) * res - atanPolyCoef1) / ((atanPolyCoef7 * res + atanPolyCoef6) * res + 1.0f);
        
        if (absY > absX)
        {
            res = (0.5f * FF2_PI) - res;
        }
        
        if (x < 0) 
        {
            res = FF2_PI - res;
        }
        
        if (y < 0) 
        {
            res = -res;
        }

        return res;
    #else
        return atan2(y, x);
    #endif
}

float ff2_acos(float x)
{
    #ifdef FF2_FAST_TRIGONOMETRY
        float xa = fabsf(x);
        float result = ff2_sqrt(1.0f - xa) * (1.5707288f + xa * (-0.2121144f + xa * (0.0742610f + (-0.0187293f * xa))));
        if (x < 0.0f)
            return FF2_PI - result;
        else
            return result;
    #else
        return acos(x);
    #endif    
}

float ff2_inv_sqrt(float x)
{
    #ifdef FF2_FAST_MATH
        float xhalf = 0.5f * x;
        int i = *(int*)&x;
        i = 0x5f375a86 - (i >> 1);
        x = *(float*)&i;
        x = x * (1.5f - xhalf * FF2_SQR(x));
        return x;
    #else
        return 1.0f / sqrtf(x);
    #endif  
}

float ff2_sqrt(float x)
{
    #ifdef FF2_FAST_MATH
        return x * ff2_inv_sqrt(x);
    #else
        return sqrtf(x);
    #endif
}

void ff2_normalize(ff2_vector3f* src, ff2_vector3f* dest)
{
    float length = ff2_magnitude(*src);
    if (length != 0.0f)
    {
        dest->x = src->x / length;
        dest->y = src->y / length;
        dest->z = src->z / length;
    }
} 

float ff2_magnitude(ff2_vector3f vector)
{
    return ff2_sqrt(FF2_SQR(vector.x) + FF2_SQR(vector.y) + FF2_SQR(vector.z));
}

void ff2_rotation_matrix(ff2_angles angles, float matrix[3][3])
{
    float cosx, sinx, cosy, siny, cosz, sinz;
    float coszcosx, sinzcosx, coszsinx, sinzsinx;

    cosx = ff2_cos(angles.roll);
    sinx = ff2_sin(angles.roll);
    cosy = ff2_cos(angles.pitch);
    siny = ff2_sin(angles.pitch);
    cosz = ff2_cos(angles.yaw);
    sinz = ff2_sin(angles.yaw);
    
    coszcosx = cosz * cosx;
    sinzcosx = sinz * cosx;
    coszsinx = sinx * cosz;
    sinzsinx = sinx * sinz;

    matrix[0][X] = cosz * cosy;
    matrix[0][Y] = -cosy * sinz;
    matrix[0][Z] = siny;
    matrix[1][X] = sinzcosx + (coszsinx * siny);
    matrix[1][Y] = coszcosx - (sinzsinx * siny);
    matrix[1][Z] = -sinx * cosy;
    matrix[2][X] = (sinzsinx) - (coszcosx * siny);
    matrix[2][Y] = (coszsinx) + (sinzcosx * siny);
    matrix[2][Z] = cosy * cosx;
}

void ff2_rotate_vector(ff2_angles angles, ff2_vector3f* vector)
{
    ff2_vector3f temp = *vector;

    float matrix[3][3];
    ff2_rotation_matrix(angles, matrix);

    vector->x = temp.x * matrix[0][X] + temp.y * matrix[0][Y] + temp.z * matrix[0][Z];
    vector->y = temp.x * matrix[1][X] + temp.y * matrix[1][Y] + temp.z * matrix[1][Z];
    vector->z = temp.x * matrix[2][X] + temp.y * matrix[2][Y] + temp.z * matrix[2][Z];
}

void ff2_rotate_vector(float matrix[3][3], ff2_vector3f* vector)
{
    float x, y, z;

    x = matrix[0][X] * vector->x + matrix[0][Y] * vector->y + matrix[0][Z] * vector->z;
    y = matrix[1][X] * vector->x + matrix[1][Y] * vector->y + matrix[1][Z] * vector->z;
    z = matrix[2][X] * vector->x + matrix[2][Y] * vector->y + matrix[2][Z] * vector->z;

    vector->x = x;
    vector->y = y;
    vector->z = z;
}

// void ff2_rotate_vector(ff2_quaternion angles, ff2_vector3f* vector)
// {
//     ff2_quaternion angles_conjugate;
//     angles_conjugate.w = angles.w;
//     angles_conjugate.x = -angles.x;
//     angles_conjugate.y = -angles.y;
//     angles_conjugate.z = -angles.z;

//     ff2_quaternion vector_extension;
//     vector_extension.w = 0.0f;
//     vector_extension.x = vector->x;
//     vector_extension.y = vector->y;
//     vector_extension.z = vector->z;

//     ff2_quaternion temporary;

//     ff2_hamilton_product(angles_conjugate, vector_extension, &temporary);
//     ff2_hamilton_product(temporary, angles, &vector_extension);

//     vector->x = vector_extension.x;
//     vector->y = vector_extension.y;
//     vector->z = vector_extension.z;
// }

// void ff2_hamilton_product(ff2_quaternion a, ff2_quaternion b, ff2_quaternion* c)
// {
//     c->w = -a.x * b.x - a.y * b.y - a.z * b.z + a.w * b.w;
//     c->x =  a.x * b.w + a.y * b.z - a.z * b.y + a.w * b.x;
//     c->y = -a.x * b.z + a.y * b.w + a.z * b.x + a.w * b.y;
//     c->z =  a.x * b.y - a.y * b.x + a.z * b.w + a.w * b.z;    
// }

float ff2_first_order_LPF_time_constant(float cutoff_frequency)
{
    return 1.0f / (FF2_TAU * cutoff_frequency);
}

float ff2_quick_median_filter_3(float* vector)
{
    float p[3];
    FF2_QMF_COPY(p, vector, 3);

    FF2_QMF_SORT(p[0], p[1]); FF2_QMF_SORT(p[1], p[2]); FF2_QMF_SORT(p[0], p[1]) ;
    return p[1];
}

float ff2_quick_median_filter_5(float* vector)
{
    float p[5];
    FF2_QMF_COPY(p, vector, 5);

    FF2_QMF_SORT(p[0], p[1]); FF2_QMF_SORT(p[3], p[4]); FF2_QMF_SORT(p[0], p[3]);
    FF2_QMF_SORT(p[1], p[4]); FF2_QMF_SORT(p[1], p[2]); FF2_QMF_SORT(p[2], p[3]);
    FF2_QMF_SORT(p[1], p[2]); 
    return p[2];
}

float ff2_quick_median_filter_7(float* vector)
{
    float p[7];
    FF2_QMF_COPY(p, vector, 7);

    FF2_QMF_SORT(p[0], p[5]); FF2_QMF_SORT(p[0], p[3]); FF2_QMF_SORT(p[1], p[6]);
    FF2_QMF_SORT(p[2], p[4]); FF2_QMF_SORT(p[0], p[1]); FF2_QMF_SORT(p[3], p[5]);
    FF2_QMF_SORT(p[2], p[6]); FF2_QMF_SORT(p[2], p[3]); FF2_QMF_SORT(p[3], p[6]);
    FF2_QMF_SORT(p[4], p[5]); FF2_QMF_SORT(p[1], p[4]); FF2_QMF_SORT(p[1], p[3]);
    FF2_QMF_SORT(p[3], p[4]);
    return p[3];
}

float ff2_quick_median_filter_9(float* vector)
{
    float p[9];
    FF2_QMF_COPY(p, vector, 9);

    FF2_QMF_SORT(p[1], p[2]); FF2_QMF_SORT(p[4], p[5]); FF2_QMF_SORT(p[7], p[8]);
    FF2_QMF_SORT(p[0], p[1]); FF2_QMF_SORT(p[3], p[4]); FF2_QMF_SORT(p[6], p[7]);
    FF2_QMF_SORT(p[1], p[2]); FF2_QMF_SORT(p[4], p[5]); FF2_QMF_SORT(p[7], p[8]);
    FF2_QMF_SORT(p[0], p[3]); FF2_QMF_SORT(p[5], p[8]); FF2_QMF_SORT(p[4], p[7]);
    FF2_QMF_SORT(p[3], p[6]); FF2_QMF_SORT(p[1], p[4]); FF2_QMF_SORT(p[2], p[5]);
    FF2_QMF_SORT(p[4], p[7]); FF2_QMF_SORT(p[4], p[2]); FF2_QMF_SORT(p[6], p[4]);
    FF2_QMF_SORT(p[4], p[2]);
    return p[4];
}

void ff2_apply_median_filter_3(float* source, float* destination, int length)
{
    for (int k = 0; k < length; k++)
    {   
        float vector[3];
        for (int j = 0; j < 3; j++)
        {
            vector[j] = source[((k - 1 + j < 0) ? 0 : ((k - 1 + j >= length) ? length - 1 : (k - 1 + j)))];
        }
        destination[k] = ff2_quick_median_filter_3(vector);
    }
}

void ff2_apply_median_filter_5(float* source, float* destination, int length)
{
    for (int k = 0; k < length; k++)
    {   
        float vector[5];
        for (int j = 0; j < 5; j++)
        {
            vector[j] = source[((k - 2 + j < 0) ? 0 : ((k - 2 + j >= length) ? length - 1 : (k - 2 + j)))];
        }
        destination[k] = ff2_quick_median_filter_5(vector);
    }
}

void ff2_apply_median_filter_7(float* source, float* destination, int length)
{
    for (int k = 0; k < length; k++)
    {   
        float vector[7];
        for (int j = 0; j < 7; j++)
        {
            vector[j] = source[((k - 3 + j < 0) ? 0 : ((k - 3 + j >= length) ? length - 1 : (k - 3 + j)))];
        }
        destination[k] = ff2_quick_median_filter_7(vector);
    }
}

void ff2_apply_median_filter_9(float* source, float* destination, int length)
{
    for (int k = 0; k < length; k++)
    {   
        float vector[9];
        for (int j = 0; j < 9; j++)
        {
            vector[j] = source[((k - 4 + j < 0) ? 0 : ((k - 4 + j >= length) ? length - 1 : (k - 4 + j)))];
        }
        destination[k] = ff2_quick_median_filter_9(vector);
    }
}

void maths_debug()
{
    ff2_printf("%i\n", ff2_constrain(5, 1, 4));
    ff2_printf("%f\n", ff2_constrainf(5.0f, 1.0f, 4.0f));        
    ff2_printf("%i\n", ff2_apply_deadband(10, 4));
    ff2_printf("%f\n", ff2_apply_deadbandf(10.0f, 4.0f));
    ff2_printf("%i\n", ff2_range_map(2, 1, 10, 11, 20));
    ff2_printf("%f\n", ff2_range_mapf(2.0f, 1.0f, 10.0f, 11.0f, 20.0f));
    ff2_printf("%f\n", ff2_sin(FF2_PI));
    ff2_printf("%f\n", ff2_cos(FF2_PI));
    ff2_printf("%f\n", ff2_tan(FF2_PI));
    ff2_printf("%f\n", ff2_atan2(1.0f, 1.0f));
    ff2_printf("%f\n", FF2_DEG(ff2_acos(0.707f)));
    ff2_printf("%f\n", ff2_inv_sqrt(2.0f));
    ff2_printf("%f\n", ff2_sqrt(2.0f));
    ff2_vector3f vector1;
    vector1.x = 1.0f;
    vector1.y = 1.0f;
    vector1.z = 1.0f;
    ff2_vector3f vector2;
    ff2_normalize(&vector1, &vector2);
    ff2_printf("%f %f %f\n", vector2.x, vector2.y, vector2.z);
    ff2_printf("%f\n", ff2_magnitude(vector2));

    float vector[4] = {2, 80, 6, 3};
    float dest[4];
    ff2_apply_median_filter_3(vector, dest, 4);
    for (int i = 0; i < 4; i++)
    {
        ff2_printf("%f\n", dest[i]);
    }
}
