#pragma once

#include "arduino_wrapper.h"
#include "persistent.h"
#include "blackbox.h"

typedef struct ff2_moving_average_def
{
	float sum = 0.0f;
	float* values;
	unsigned size = 0;
	float average = 0.0f;
	unsigned long entry_counter = 0;
} ff2_moving_average;

void ff2_moving_average_initialize(ff2_moving_average* data, unsigned size);
void ff2_moving_average_push(ff2_moving_average* data, float value);
void ff2_moving_average_destroy(ff2_moving_average* data);
void ff2_moving_average_debug();
