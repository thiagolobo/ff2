#pragma once

/*
	DATASHEET:
	https://cdn-shop.adafruit.com/datasheets/BST-BMP180-DS000-09.pdf

	GITHUB WITH REFERENCE CODE:
	https://github.com/BoschSensortec/BMP180_driver
*/

#include "i2c_device.h"

#define BMP180_ADDRESS 0x77

#define BMP180_ULTRALOWPOWER 	((uint8_t) 0)
#define BMP180_STANDARD      	((uint8_t) 1)
#define BMP180_HIGHRES       	((uint8_t) 2)
#define BMP180_ULTRAHIGHRES  	((uint8_t) 3)

#define BMP180_RAW_CALLIBRATION_SIZE 22

// Callibration data addresses (2 bytes each)
#define BMP180_CAL_AC1     		0xAA
#define BMP180_CAL_AC2         	0xAC
#define BMP180_CAL_AC3          0xAE
#define BMP180_CAL_AC4          0xB0
#define BMP180_CAL_AC5          0xB2
#define BMP180_CAL_AC6          0xB4
#define BMP180_CAL_B1           0xB6
#define BMP180_CAL_B2           0xB8
#define BMP180_CAL_MB           0xBA
#define BMP180_CAL_MC           0xBC
#define BMP180_CAL_MD           0xBE

#define BMP180_CONTROL          0xF4 
#define BMP180_TEMPDATA         0xF6
#define BMP180_PRESSUREDATA     0xF6
#define BMP180_READTEMPCMD      0x2E
#define BMP180_READPRESSURECMD  0x34

#define BMP180_SAMPLING_RATE 	BMP180_ULTRAHIGHRES

uint8_t oversampling;
int16_t ac1, ac2, ac3, b1, b2, mb, mc, md;
uint16_t ac4, ac5, ac6;
uint16_t raw_temperature;
uint32_t raw_pressure;
unsigned step = 0;

int auxiliary_array[2];

ff2_moving_average ff2_altitude;
float ff2_temperature;
float ff2_pressure = 0.0f;
float last_read_time;
float raw_pressure_wait_time;

void ff2_bmp180_initialize();
void ff2_bmp180_update();

void ff2_bmp180_read_raw_temperature();
void ff2_bmp180_read_raw_pressure();

int32_t ff2_bmp180_compute_B5();