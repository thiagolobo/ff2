#include "persistent.h"

void ff2_persistent_initialize()
{
#ifdef PERSISTENT_ENABLED
	ff2_delay(INITIALIZATION_TIME);
	ff2_debug_printf(">> Initializing Persistent Flash Space...\n");

	byte has_data = ff2_flash_read(PERSISTENT_OFFSET_HAS_DATA);

	if (has_data != 1)
	{		
		ff2_debug_printf("No persistent data found (has_data = %i). Writing from default values...\n", has_data);
		persistent_has_data = false;
		// Write header data...
		ff2_flash_write(PERSISTENT_OFFSET_HAS_DATA, 1);
		ff2_flash_write(PERSISTENT_OFFSET_DATA_COUNT, PERSISTENT_DATA_COUNT);		
	}
	else
	{
		ff2_debug_printf("Persistent data found (has_data = %i). Loading...\n", has_data);
		persistent_has_data = true;
	}
#endif
}

void ff2_persistent_write_int(unsigned offset, int value)
{
#ifdef PERSISTENT_ENABLED
	byte bytes[PERSISTENT_DATA_SIZE];
	memcpy(bytes, &value, sizeof(int));
	ff2_flash_write(offset, bytes, PERSISTENT_DATA_SIZE);
#endif
}

void ff2_persistent_write_float(unsigned offset, float value)
{
#ifdef PERSISTENT_ENABLED
	byte bytes[PERSISTENT_DATA_SIZE];
	memcpy(bytes, &value, sizeof(float));
	ff2_flash_write(offset, bytes, PERSISTENT_DATA_SIZE);	
#endif
}

int ff2_persistent_read_int(unsigned offset)
{	
#ifdef PERSISTENT_ENABLED
	byte* bytes = ff2_flash_read_address(offset);
	int read = -1;
	memcpy(&read, bytes, PERSISTENT_DATA_SIZE);
	return read;
#else
	return 0;
#endif
}

float ff2_persistent_read_float(unsigned offset)
{
#ifdef PERSISTENT_ENABLED
	byte* bytes = ff2_flash_read_address(offset);
	float read = -1.0f;
	memcpy(&read, bytes, PERSISTENT_DATA_SIZE);
	return read;
#else
	return 0.0f;
#endif
}

// Next 4 byte multiple
unsigned ff2_persistent_header_length()
{
	return (PERSISTENT_LENGTH_HEADER + 3) & ~0x03;
}

unsigned ff2_persistent_length()
{
	return ff2_persistent_header_length() + PERSISTENT_DATA_COUNT * PERSISTENT_DATA_SIZE;
}

void ff2_persistent_debug()
{
	ff2_printf(">> Initializing Persistent Flash debug...\n");
	ff2_printf("Length: %i\n", ff2_persistent_length());	

	ff2_persistent_write_int(8, -327);

	ff2_printf("Read back from memory: %i\n", ff2_persistent_read_int(8));
}


// if (ff2_time() - last_time >= 20.0f)
// {
// 	ff2_debug_printf("Dumping blackbox...\n");
// // 	ff2_blackbox_write_int(counter++, 0xaa);
// 	lastTime = ff2_time();
// // 	if (counter % 4 == 0 && counter != 0)
// // 	{
// // 		// counter = 0;
// 		ff2_blackbox_dump();
// // 	}
// }

// if (Serial.available()) 
// {
// 	byte byteRead = Serial.read();

// 	if (byteRead == 'q') 
// 	{
// 		// ff2_printf("testando!\n");
// 		ff2_flash_dump();
// 	}
// }