#pragma once

#include "arduino_wrapper.h"
#include "flash.h"

// #define PERSISTENT_ENABLED

#define PERSISTENT_OFFSET_HAS_DATA 		0
#define PERSISTENT_OFFSET_DATA_COUNT 	1
#define PERSISTENT_LENGTH_HEADER 		2

// Update these for every new piece of persistent data
#define PERSISTENT_DATA_COUNT 36
#define PERSISTENT_DATA_SIZE  4 // bytes

// Define other persistent data here
#define PERSISTENT_OFFSET_BLACKBOX_OFFSET 		(ff2_persistent_header_length() + 0 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_BLACKBOX_FLIGHT_ID	(ff2_persistent_header_length() + 1 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_VOLTIMETER_THRESHOLD	(ff2_persistent_header_length() + 2 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_VOLTIMETER_BBP        (ff2_persistent_header_length() + 3 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_AHRS_KP				(ff2_persistent_header_length() + 4 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_AHRS_KI				(ff2_persistent_header_length() + 5 * PERSISTENT_DATA_SIZE)
// Roll Angular Speed PID
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_KP	(ff2_persistent_header_length() + 6 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_KI (ff2_persistent_header_length() + 7 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_KD (ff2_persistent_header_length() + 8 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_INTEGRAL_CONSTRAINT (ff2_persistent_header_length() + 9 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_OUTPUT_CONSTRAINT (ff2_persistent_header_length() + 10 * PERSISTENT_DATA_SIZE)
// Pitch Angular Speed PID
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_KP (ff2_persistent_header_length() + 11 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_KI (ff2_persistent_header_length() + 12 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_KD (ff2_persistent_header_length() + 13 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_INTEGRAL_CONSTRAINT (ff2_persistent_header_length() + 14 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_OUTPUT_CONSTRAINT (ff2_persistent_header_length() + 15 * PERSISTENT_DATA_SIZE)
// Yaw Angular Speed PID
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_KP (ff2_persistent_header_length() + 16 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_KI (ff2_persistent_header_length() + 17 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_KD (ff2_persistent_header_length() + 18 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_INTEGRAL_CONSTRAINT (ff2_persistent_header_length() + 19 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_OUTPUT_CONSTRAINT (ff2_persistent_header_length() + 20 * PERSISTENT_DATA_SIZE)
// Roll Angle PID
#define PERSISTENT_OFFSET_PID_ANGLE_ROLL_KP	(ff2_persistent_header_length() + 21 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGLE_ROLL_KI (ff2_persistent_header_length() + 22 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGLE_ROLL_KD (ff2_persistent_header_length() + 23 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGLE_ROLL_INTEGRAL_CONSTRAINT (ff2_persistent_header_length() + 24 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGLE_ROLL_OUTPUT_CONSTRAINT (ff2_persistent_header_length() + 25 * PERSISTENT_DATA_SIZE)
// Pitch Angle PID
#define PERSISTENT_OFFSET_PID_ANGLE_PITCH_KP (ff2_persistent_header_length() + 26 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGLE_PITCH_KI (ff2_persistent_header_length() + 27 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGLE_PITCH_KD (ff2_persistent_header_length() + 28 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGLE_PITCH_INTEGRAL_CONSTRAINT (ff2_persistent_header_length() + 29 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGLE_PITCH_OUTPUT_CONSTRAINT (ff2_persistent_header_length() + 30 * PERSISTENT_DATA_SIZE)
// Pitch Angle PID
#define PERSISTENT_OFFSET_PID_ANGLE_YAW_KP (ff2_persistent_header_length() + 31 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGLE_YAW_KI (ff2_persistent_header_length() + 32 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGLE_YAW_KD (ff2_persistent_header_length() + 33 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGLE_YAW_INTEGRAL_CONSTRAINT (ff2_persistent_header_length() + 34 * PERSISTENT_DATA_SIZE)
#define PERSISTENT_OFFSET_PID_ANGLE_YAW_OUTPUT_CONSTRAINT (ff2_persistent_header_length() + 35 * PERSISTENT_DATA_SIZE)

bool persistent_has_data = false;

void ff2_persistent_initialize();

void ff2_persistent_write_int(unsigned offset, int value);
void ff2_persistent_write_float(unsigned offset, float value);
int ff2_persistent_read_int(unsigned offset);
float ff2_persistent_read_float(unsigned offset);

unsigned ff2_persistent_header_length();
unsigned ff2_persistent_length();
void ff2_persistent_debug();
