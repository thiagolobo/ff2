#include "behavior.h"

void ff2_behavior_initialize()
{
	// Initialization function calls should be put here
	ff2_i2cd_initialize();
	DEFAULT_SERIAL.begin(115200);

	ff2_bluetooth_initialize();
	ff2_led_initialize();
	ff2_flash_initialize();
	ff2_persistent_initialize();
	ff2_blackbox_initialize();
	ff2_radio_initialize();
  	ff2_motor_initialize();
  	ff2_voltimeter_initialize();
  	ff2_mpu6050_initialize();
  	// ff2_compass_initialize();
  	ff2_gps_initialize();
  	ff2_ahrs_initialize();
  	ff2_pid_initialize();
  	
	ff2_debug_printf("RAM: %i bytes.\n", ff2_ram());

	ff2_synchronizer = 0;
	ff2_led_set_blink(LED_BLUE, 1.0f, 1.0f);
	behavior_last_time = ff2_time();
}

void ff2_behavior_update()
{
	float dt = ff2_time() - behavior_last_time;

	if (dt >= 1.0f / BEHAVIOR_UPDATE_RATE)
	{
		// Poll rate control. Obtain dt and accumulate elapsed time
		behavior_last_time = ff2_time();
		behavior_current_time = behavior_last_time;
		behavior_accumulator += dt;

		// Update function calls should be put here
		ff2_bluetooth_update();
		if (ff2_synchronizer % 100 == 0)
		{
			ff2_voltimeter_update();
		}
		ff2_led_update();
		if (ff2_synchronizer % 16 == 0)
		{
			ff2_compass_update();	
		}
		ff2_gps_update();
		ff2_mpu6050_update();
		ff2_ahrs_update(dt);

		// Check current state

		// Mover isso!
		float roll_angle_output = 0.0f;
		float pitch_angle_output = 0.0f;
		float yaw_angle_output = 0.0f;

		float roll_angular_speed_output = 0.0f;
		float pitch_angular_speed_output = 0.0f;
		float yaw_angular_speed_output = 0.0f;
		// Mover isso!

		// Wait for safe throttle position after initialization and arm
		if (ff2_behavior_state == BEHAVIOR_STATE_INITIALIZATION)
		{
			ff2_led_set_blink(LED_ORANGE, 0.1f, 0.1f);
			ff2_behavior_wait_and_arm();
		}

		// Safe mode, activated on ground
		if (ff2_behavior_state == BEHAVIOR_STATE_SAFE)
		{
			ff2_motor_set_throttle(MOTOR_FRONT_L, MOTOR_MIN_THROTTLE);
			ff2_motor_set_throttle(MOTOR_FRONT_R, MOTOR_MIN_THROTTLE);
			ff2_motor_set_throttle(MOTOR_BACK_L, MOTOR_MIN_THROTTLE);
			ff2_motor_set_throttle(MOTOR_BACK_R, MOTOR_MIN_THROTTLE);			

			if (ff2_radio_chan_value(DIAL_LEFT) <= 0.3f)
			{
				ff2_behavior_state = BEHAVIOR_STATE_ARMED;
				ff2_led_set_fixed(LED_GREEN, 0);
			}			
		}

		// Armed mode, ready to receive radio input and run the control pipeline
		if (ff2_behavior_state == BEHAVIOR_STATE_ARMED)
		{
			if (ff2_radio_chan_value(DIAL_LEFT) > 0.3f)
			{
				ff2_behavior_state = BEHAVIOR_STATE_SAFE;
				ff2_led_set_fixed(LED_GREEN, 255);
			}

			// Control Pipeline
			if (behavior_flight_mode == BEHAVIOR_FLIGHT_ACCRO)
			{
				// Rate PID
			}

			if (behavior_flight_mode == BEHAVIOR_FLIGHT_LEVEL)
			{
				// Calculate incremental yaw setpoint
				yaw_angle_setpoint -= ff2_radio_chan_value(RUDDER) * yaw_angle_command_scale;
				// yaw_angle_setpoint = 90.0f;
				if (yaw_angle_setpoint >= 180.0f)
				{
					yaw_angle_setpoint -= 360.0f;
				} 
				else if (yaw_angle_setpoint < -180.0f)
				{
					yaw_angle_setpoint += 360.0f;
				}

				// Correct for shortest yaw path
				float attitude_yaw_corrected = ff2_attitude.yaw;
				if (FF2_ABS(yaw_angle_setpoint - ff2_attitude.yaw) >= 180.0f)
				{
					if (ff2_attitude.yaw >= 0.0f)
					{
						attitude_yaw_corrected -= 360.0f;
					}
					else
					{
						attitude_yaw_corrected += 360.0f;
					}
				}

				// Angle PID				
				roll_angle_output = ff2_pid_run(PID_ANGLE_ROLL, ff2_radio_chan_value(AILERON) * roll_pitch_angle_command_scale, ff2_attitude.roll, ff2_gyro_data.x, dt);
				pitch_angle_output = ff2_pid_run(PID_ANGLE_PITCH, ff2_radio_chan_value(ELEVATOR) * roll_pitch_angle_command_scale, ff2_attitude.pitch, ff2_gyro_data.y, dt);
				yaw_angle_output = ff2_pid_run(PID_ANGLE_YAW, yaw_angle_setpoint, attitude_yaw_corrected, ff2_gyro_data.z, dt);
								
				float throttle_offset = ff2_range_mapf(ff2_radio_chan_value(THROTTLE), 0.0f, 1.0f, MOTOR_MIN_THROTTLE, MOTOR_MAX_THROTTLE - 250.0f);				

				ff2_motor_set_throttle(MOTOR_FRONT_L, (int) (throttle_offset + 0.7071f * roll_angle_output - 0.7071f * pitch_angle_output + yaw_angle_output));
				ff2_motor_set_throttle(MOTOR_FRONT_R, (int) (throttle_offset - 0.7071f * roll_angle_output - 0.7071f * pitch_angle_output - yaw_angle_output));
				ff2_motor_set_throttle(MOTOR_BACK_L, (int)  (throttle_offset + 0.7071f * roll_angle_output + 0.7071f * pitch_angle_output - yaw_angle_output));
				ff2_motor_set_throttle(MOTOR_BACK_R, (int)  (throttle_offset - 0.7071f * roll_angle_output + 0.7071f * pitch_angle_output + yaw_angle_output));
			}

			if (behavior_flight_mode == BEHAVIOR_FLIGHT_HORIZON)
			{
				// Angle PID
				// Rate PID
			}

			if (behavior_flight_mode == BEHAVIOR_FLIGHT_ALTITUDE_HOLD)
			{
				// Position PID (z)
				// Velocity PID (z)
				// Angle PID
				// Rate PID	
			}

			if (behavior_flight_mode == BEHAVIOR_FLIGHT_POSITION_HOLD)
			{
				// Position PID (x, y, z)
				// Velocity PID (x, y, z)
				// Angle PID
				// Rate PID
			}		

			if (behavior_flight_mode == BEHAVIOR_FLIGHT_AUTONAV)
			{
				// Navigation PID
				// Position PID (x, y, z)
				// Velocity PID (x, y, z)
				// Angle PID
				// Rate PID
			}
		}

		// Behavior related prints/led toggles should be put here
		if (ff2_synchronizer % 3000 == 0)
		{
			if (ff2_synchronizer != 0)
			{
				ff2_debug_printf("Mean loop time: %f\n", behavior_accumulator / ff2_synchronizer);
			}			
		}

		if (ff2_synchronizer % 200 == 0)
		{
			// ff2_printf("------------\n");
			// ff2_debug_printf("Sat: %i\n", ff2_gps_satellite_count);
			// ff2_debug_printf("Fix: %d Heading: %f G_Speed: %f\n", ff2_gps_fixed, ff2_gps_heading, ff2_gps_ground_speed);
			// ff2_debug_printf("Lon: %f Lat: %f Alt: %f\n", ff2_gps_position.x, ff2_gps_position.y, ff2_gps_position.z);
			// ff2_debug_printf("vx: %f vy: %f vz: %f\n", ff2_gps_velocity.x, ff2_gps_velocity.y, ff2_gps_velocity.z);
			// ff2_debug_printf("Y: %f S: %f\n", ff2_attitude.yaw, yaw_angle_setpoint);
			// ff2_radio_debug();	
			// ff2_printf("Attitude: %f, %f, %f\n", ff2_attitude.roll, ff2_attitude.pitch, ff2_attitude.yaw);
			// ff2_printf("Gyro_minmax: %f | %f, %f | %f, %f | %f \n", gyro_x_min, gyro_x_max, gyro_y_min, gyro_y_max, gyro_z_min, gyro_z_max);
			// ff2_printf("Accl_minmax: %f | %f, %f | %f, %f | %f \n", accel_x_min, accel_x_max, accel_y_min, accel_y_max, accel_z_min, accel_z_max);
			// ff2_printf("Gyro______: %f, %f, %f \n", ff2_gyro_data.x, ff2_gyro_data.y, ff2_gyro_data.z);
			// ff2_printf("Accel_____: %f, %f, %f \n", ff2_accel_data.x, ff2_accel_data.y, ff2_accel_data.z);
			// ff2_printf("SET: %f Roll: %f PIDa: %f Gx: %f PIDv: %f\n", ff2_radio_chan_value(AILERON) * roll_pitch_angle_command_scale, ff2_attitude.roll, roll_angle_output, ff2_gyro_data.x, roll_angular_speed_output);
			// ff2_printf("SET: %f Pitch: %f PIDa: %f Gy: %f PIDv: %f\n", ff2_radio_chan_value(ELEVATOR) * roll_pitch_angle_command_scale, ff2_attitude.pitch, pitch_angle_output, ff2_gyro_data.y,  pitch_angular_speed_output);
			// ff2_printf("SET: %f Yaw: %f PIDa: %f Gz: %f PIDv: %f\n", yaw_angle_setpoint, ff2_attitude.yaw, yaw_angle_output, ff2_gyro_data.z, yaw_angular_speed_output);
		}

		++ff2_synchronizer;
		ff2_behavior_self_check();
	}
}

void ff2_behavior_self_check()
{
	// Avoid overflow (Boeing 787)
	if (ff2_synchronizer >= BEHAVIOR_UPDATE_RATE * 2500)
	{
		behavior_accumulator = 0.0f;
		ff2_synchronizer = 0;
	}
}

void ff2_behavior_wait_and_arm()
{
	// TIRAR ISSO!
	// if (ff2_ahrs_ready)
	// {
	// 	ff2_printf(">> Skipping security lock...\n");
	// 	ff2_behavior_state = BEHAVIOR_STATE_ARMED;	
	// 	yaw_angle_setpoint = ff2_attitude.yaw;
	// 	return;
	// }
	// TIRAR ISSO!

	static unsigned rudder_counter = 0;
	static unsigned aileron_counter = 0;

	static float last_rudder_command = 0.0f;
	static float last_aileron_command = 0.0f;

	float rudder_command = ff2_radio_chan_value(RUDDER);
	float aileron_command = ff2_radio_chan_value(AILERON);

	// Also wait for ahrs
	if (FF2_ABS(rudder_command - last_rudder_command) >= 0.2f || rudder_command == 0.0f ||
		FF2_ABS(aileron_command - last_aileron_command) >= 0.2f || aileron_command == 0.0f ||
		!ff2_ahrs_ready)
	{
		rudder_counter = 0;
		aileron_counter = 0;
	}
	else
	{
		++rudder_counter;
		++aileron_counter;
	}

	last_rudder_command = rudder_command;
	last_aileron_command = aileron_command;

	if (rudder_counter >= 1000 && aileron_counter >= 1000)
	{
		if (rudder_command <= -0.5f)
		{			
			ff2_behavior_state = BEHAVIOR_STATE_ARMED;	
			ff2_printf(">> Armed!\n");
			yaw_angle_setpoint = ff2_attitude.yaw;
			ff2_led_set_fixed(LED_ORANGE, 0);
		}			
	}

}
