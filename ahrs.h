#pragma once

#include "arduino_wrapper.h"
#include "i2c_device.h"
#include "maths.h"

#include "mpu6050.h"
#include "compass.h"

#define AHRS_SPIN_RATE_LIMIT 20
#define AHRS_MEAN_DEVIATION_SIZE 10 // 5 seconds of low deviation
#define AHRS_MEAN_DEVIATION_THRESHOLD 0.1f
#define MAGNETIC_DECLINATION 0.0f

ff2_angles last_attitude;
ff2_angles ff2_attitude;

float ahrs_kp_target = 2.0f;
float ahrs_ki_target = 0.01f;

// Initial values
float ahrs_kp = 125.0f; 
float ahrs_ki = 125.0f; 

ff2_quaternion ahrs_attitude_quaternion;

float ahrs_rotation_matrix[3][3];
float ahrs_integralFBx = 0.0f,  ahrs_integralFBy = 0.0f, ahrs_integralFBz = 0.0f;
bool ff2_ahrs_ready = false;

ff2_moving_average mean_deviation_roll;
ff2_moving_average mean_deviation_pitch;
ff2_moving_average mean_deviation_yaw;

ff2_vector3f ff2_accel_data_earth;

void ff2_ahrs_initialize();

void ff2_ahrs_mahony_update(float gx, float gy, float gz, bool useAcc, float ax, float ay, float az, bool useMag, float mx, float my, float mz, bool useYaw, float yawError, float dt);
void ff2_ahrs_compute_rotation_matrix();
void ff2_ahrs_euler_update();
void ff2_ahrs_acceleration_update();

void ff2_ahrs_update(float dt);

void ff2_ahrs_set_kp(float value);
void ff2_ahrs_set_ki(float value);