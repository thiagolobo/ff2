#include "gps.h"

void ff2_gps_initialize()
{
    ff2_delay(INITIALIZATION_TIME);
    ff2_debug_printf(">> Initializing GPS...\n");

    GPS_SERIAL.begin(9600);
    ff2_debug_printf("Deactivating NMEA protocol...\n");
    ff2_debug_printf("Activating UBX protocol...\n");
    ff2_debug_printf("Setting frequency and baud rate...\n");

    // Write initialization bytes
    for (int i = 0; i < sizeof(ublox_init) / sizeof(char); i++)
    {
        GPS_SERIAL.write(ublox_init[i]);
        ff2_delay(10);
    }

    GPS_SERIAL.end();
    GPS_SERIAL.begin(38400);       
}

void ff2_gps_update()
{
    while (GPS_SERIAL.available())
    {
        uint8_t received_byte = GPS_SERIAL.read();
        
        switch (decode_step)
        {
            case 1:
                // UBX Header
                if (PREAMBLE_2 == received_byte) 
                {
                    ++decode_step;
                    break;
                }
                decode_step = 0;
            case 0:
                // UBX Header
                if (PREAMBLE_1 == received_byte) 
                {
                    ++decode_step;
                }
                break;            
            case 2:
                // Message Class (and initialize checksums)
                ++decode_step;
                message_class = received_byte;
                checksum_a = received_byte;
                checksum_b = received_byte;
                break;
            case 3:
                // Message ID
                ++decode_step;
                checksum_a += received_byte;
                checksum_b += checksum_a;
                message_id = received_byte;
                break;
            case 4:
                // payload_length LSByte
                ++decode_step;
                checksum_a += received_byte;
                checksum_b += checksum_a;
                payload_length = received_byte;
                break;
            case 5:
                // payload_length MSByte
                ++decode_step;
                checksum_a += received_byte;
                checksum_b += checksum_a;
                payload_length += (uint16_t) (received_byte << 8);
                if (payload_length > 512) 
                {
                    payload_length = 0;
                    decode_step = 0;
                }
                payload_counter = 0;
                break;
            case 6:
                // Fill 'buffer' with received_byte and keep decode_step frozen until all received_byte is read
                checksum_a += received_byte;
                checksum_b += checksum_a;
                if (payload_counter < sizeof(buffer)) 
                {
                    buffer.bytes[payload_counter] = received_byte;
                }
                if (++payload_counter == payload_length) 
                {
                    ++decode_step;
                }
                break;
            case 7:
                // Test checksum_a
                ++decode_step;
                if (checksum_a != received_byte) {
                    ff2_debug_printf("Checksum A error!\n");
                    decode_step = 0;
                }
            case 8:
                // Test checksum_b
                decode_step = 0;
                if (checksum_b != received_byte) 
                {
                    // break;
                }
                // Parse the message
                ff2_gps_parse(message_id);
        }
    }
}

void ff2_gps_parse(byte message_id)
{
    switch (message_id)
    {
        case MSG_POSLLH:
            if (ff2_gps_fixed) 
            {
                ff2_gps_position.x = buffer.posllh.longitude / 10000000.0f;
                ff2_gps_position.y = buffer.posllh.latitude / 10000000.0f;
                ff2_gps_position.z = buffer.posllh.altitude_msl / 1000.0f;
            }            
            break;
        case MSG_SOL:
            ff2_gps_fixed = false;
            if ((buffer.solution.fix_status & NAV_STATUS_FIX_VALID) &&
                (buffer.solution.fix_type == FIX_3D ||
                 buffer.solution.fix_type == FIX_2D))
            {
                ff2_gps_fixed = true;    
            }
            ff2_gps_satellite_count = buffer.solution.satellites;
            break;
        case MSG_VELNED:
            ff2_gps_velocity.x = buffer.velned.ned_north * 0.01f;
            ff2_gps_velocity.y = buffer.velned.ned_east * 0.01f;
            ff2_gps_velocity.z = buffer.velned.ned_down * 0.01f;
            ff2_gps_ground_speed = buffer.velned.speed_2d * 0.01f;
            ff2_gps_heading = buffer.velned.heading_2d / 100000.0f;
            break;
        default:
            break;
    }
}
