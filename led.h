#pragma once

#include "arduino_wrapper.h"
#include "math.h"
#include "maths.h"

#define LED_FIXED 0
#define LED_BLINK 1

#define LED_MAX_COUNT 4

#define LED_BLUE_PORT	13
#define LED_ORANGE_PORT	12
#define LED_GREEN_PORT	11
#define LED_RED_PORT	10

#define LED_BLUE 	0 
#define LED_ORANGE 	1
#define LED_GREEN 	2
#define LED_RED 	3

typedef struct ff2_led_def
{
	byte port;
	byte mode = LED_FIXED;
	bool state = false;
	byte brightness = 0;
	float blink_period_low = 0.5f;
	float blink_period_high = 0.5f;
	float last_time;
} ff2_led;

ff2_led led_array[LED_MAX_COUNT];
unsigned long led_accumulated_time;
float led_scale = 1.0f;

void ff2_led_initialize();
void ff2_led_fast_update();
void ff2_led_update();
void ff2_led_set_fixed(byte led, byte brightness);
void ff2_led_set_blink(byte led, float blink_period_low, float blink_period_high);
void ff2_led_debug();
void ff2_led_set_scale(float value);
