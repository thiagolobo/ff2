#pragma once

#include "maths.h"
#include "arduino_wrapper.h"

#define AILERON 	0
#define ELEVATOR 	1
#define RUDDER		2
#define THROTTLE	3
#define DIAL_LEFT	4
#define DIAL_RIGHT	5

#define RADIO_CHAN_1 30
#define RADIO_CHAN_2 31
#define RADIO_CHAN_3 32
#define RADIO_CHAN_4 33
#define RADIO_CHAN_5 34
#define RADIO_CHAN_6 35

#define RADIO_CHAN_COUNT 6

byte radio_mapping[RADIO_CHAN_COUNT];

byte radio_ports[RADIO_CHAN_COUNT];

long radio_timers[RADIO_CHAN_COUNT];
long radio_widths[RADIO_CHAN_COUNT];
long radio_max[RADIO_CHAN_COUNT];
long radio_min[RADIO_CHAN_COUNT];

void ff2_radio_initialize();

float ff2_radio_chan_value(byte channel);
void ff2_radio_debug();
void ff2_interrupt(byte channel);

void ff2_interrupt_0();
void ff2_interrupt_1();
void ff2_interrupt_2();
void ff2_interrupt_3();
void ff2_interrupt_4();
void ff2_interrupt_5();
