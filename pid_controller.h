#pragma once

#define PID_COUNT 6

#define PID_ANGULAR_SPEED_ROLL 		0
#define PID_ANGULAR_SPEED_PITCH		1
#define PID_ANGULAR_SPEED_YAW		2
#define PID_ANGLE_ROLL 				3
#define PID_ANGLE_PITCH				4
#define PID_ANGLE_YAW				5

typedef struct ff2_pid_data_def
{
	float KP = 0.0f; // Settable
	float KI = 0.0f; // Settable
	float KD = 0.0f; // Settable

	bool use_setpoint_rate = false; // Settable
	float setpoint_rate = 0.0f;
	float last_setpoint = 0.0f;
	float setpoint_LPF_time_constant = 0.0f; // Settable
	float setpoint_constraint = 80.0f; // Settable

	bool use_rate_feedback = false;
	float balance_factor = 5.2f; // Settable

	float last_error = 0.0f;
	float derivative_LPF_time_constant = 0.0f; // Settable
	float derivative = 0.0f;
	float integral = 0.0f;
	float integral_constraint = 100.0f; // Settable
	float output_constraint = 0.0f; // Settable
} ff2_pid_data;

ff2_pid_data ff2_pid_array[PID_COUNT];

void ff2_pid_initialize();
float ff2_pid_run(byte pid_id, float setpoint, float measurement, float measurement_rate, float dt);
