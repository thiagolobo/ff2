/* 
DueFlashStorage saves non-volatile data for Arduino Due.
The library is made to be similar to EEPROM library
Uses flash block 1 per default.

Note: uploading new software will erase all flash so data written to flash
using this library will not survive a new software upload. 

Inspiration from Pansenti at https://github.com/Pansenti/DueFlash
Rewritten and modified by Sebastian Nilsson
*/

#pragma once

#include <Arduino.h>
#include "flash_efc.h"
#include "efc.h"
#include "profiler.h"

// 1Kb of data
#define FLASH_DATA_LENGTH   ((IFLASH1_PAGE_SIZE / sizeof(byte)) * 4)

// choose a start address that's offset to show that it doesn't have to be on a page boundary
#define  FLASH_START  ((byte*) IFLASH1_ADDR)
#define FLASH_SIZE 262144

//  FLASH_DEBUG can be enabled to get debugging information displayed.
#define FLASH_DEBUG
#ifdef FLASH_DEBUG
	#define _FLASH_DEBUG(x) Serial.print(x);
#else
	#define _FLASH_DEBUG(x)
#endif

void ff2_flash_initialize();
byte ff2_flash_read(unsigned address);
byte* ff2_flash_read_address(unsigned address);
bool ff2_flash_write(unsigned address, byte value);
bool ff2_flash_write(unsigned address, byte* data, unsigned data_length);
void ff2_flash_debug();
void ff2_flash_dump();
