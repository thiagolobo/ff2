#include "flash.h"

void ff2_flash_initialize() 
{
    ff2_delay(INITIALIZATION_TIME);
    ff2_debug_printf(">> Initializing flash memory...\n");
    uint32_t retCode;
    /* Initialize flash: 6 wait states for flash writing. */
    retCode = flash_init(FLASH_ACCESS_MODE_128, 6);
    if (retCode != FLASH_RC_OK) 
    {
        _FLASH_DEBUG("Flash init failed\n");
    }
}

byte ff2_flash_read(unsigned address) 
{
    return FLASH_START[address];
}

byte* ff2_flash_read_address(unsigned address) 
{
    return FLASH_START + address;
}

bool ff2_flash_write(unsigned address, byte value) 
{
    uint32_t retCode;
    uint32_t byteLength = 1;
    byte *data;

    retCode = flash_unlock((uint32_t)FLASH_START + address, (uint32_t) FLASH_START + address + byteLength - 1, 0, 0);
    if (retCode != FLASH_RC_OK) 
    {
        _FLASH_DEBUG("Failed to unlock flash for write\n");
        return false;
    }

    // write data
    retCode = flash_write((uint32_t) FLASH_START + address, &value, byteLength, 1);
    //retCode = flash_write((uint32_t)FLASH_START, data, byteLength, 1);

    if (retCode != FLASH_RC_OK) 
    {
        _FLASH_DEBUG("Flash write failed\n");
        return false;
    }

    // Lock page
    retCode = flash_lock((uint32_t)FLASH_START+address, (uint32_t)FLASH_START+address + byteLength - 1, 0, 0);
    if (retCode != FLASH_RC_OK) 
    {
        _FLASH_DEBUG("Failed to lock flash page\n");
        return false;
    }

    return true;
}

bool ff2_flash_write(unsigned address, byte *data, unsigned dataLength) 
{
    uint32_t retCode;

    if ((uint32_t) FLASH_START + address < IFLASH1_ADDR) {
        _FLASH_DEBUG("Flash write address too low\n");
        return false;
    }

    if ((uint32_t) FLASH_START + address >= (IFLASH1_ADDR + IFLASH1_SIZE)) {
        _FLASH_DEBUG("Flash write address too high\n");
        return false;
    }

    if (((uint32_t) FLASH_START + address & 3) != 0) {
        _FLASH_DEBUG("Flash start address must be on four byte boundary\n");
        return false;
    }

    // Unlock page
    retCode = flash_unlock((uint32_t) FLASH_START + address, (uint32_t)FLASH_START + address + dataLength - 1, 0, 0);
    if (retCode != FLASH_RC_OK) {
        _FLASH_DEBUG("Failed to unlock flash for write\n");
        return false;
    }

    // write data
    retCode = flash_write((uint32_t) FLASH_START + address, data, dataLength, 1);

    if (retCode != FLASH_RC_OK) {
        _FLASH_DEBUG("Flash write failed\n");
        return false;
    }

    // Lock page
    retCode = flash_lock((uint32_t) FLASH_START + address, (uint32_t) FLASH_START + address + dataLength - 1, 0, 0);
    if (retCode != FLASH_RC_OK) {
        _FLASH_DEBUG("Failed to lock flash page\n");
        return false;
    }
    
    return true;
}

void ff2_flash_debug()
{
    ff2_printf(">> Starting Flash Memory Debug...\n");
    
    // unsigned content_size = 256;

    // byte content[content_size];
    // ff2_printf("Content size: %i\n", sizeof(content));

    // for (unsigned i = 0; i < content_size; i++)
    // {
    //     content[i] = i & 0xff;
    // }

    // ff2_flash_write(0, content, sizeof(content));

    ff2_flash_dump();

    // ff2_profiler_start();
    // for (unsigned i = 0; i < 1024; i++)
    // {
    //     ff2_flash_write(i * content_size, content, sizeof(content));
    //     ff2_profiler_update();
    // }
    // ff2_profiler_end();    
}

//Can write 1024 * 256 bytes = 256 kB
void ff2_flash_dump()
{
    ff2_printf(">> Dumping Flash Memory...\n");
    byte* flash_bytes = ff2_flash_read_address(0);
    unsigned i = 0;

    ff2_printf("Persistent Header Section:\n");
    for (i = 0; i < ff2_persistent_header_length(); i++)
    {
        ff2_printf("%i: %02x\n", i, flash_bytes[i]);
    }
    
    ff2_printf("Persistent Data Section:\n");
    for (; i < ff2_persistent_length(); i++)
    {
        ff2_printf("%i: %02x\n", i, flash_bytes[i]);
    }
    
    ff2_printf("Blackbox Section:\n");
    for (; i < ff2_persistent_read_int(PERSISTENT_OFFSET_BLACKBOX_OFFSET); i += 20)
    {
        ff2_printf("%i:\t %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x %02x\n", i, flash_bytes[i], flash_bytes[i + 1], 
                                                                                                                                     flash_bytes[i + 2], flash_bytes[i + 3], 
                                                                                                                                     flash_bytes[i + 4], flash_bytes[i + 5], 
                                                                                                                                     flash_bytes[i + 6], flash_bytes[i + 7], 
                                                                                                                                     flash_bytes[i + 8], flash_bytes[i + 9], 
                                                                                                                                     flash_bytes[i + 10], flash_bytes[i + 11], 
                                                                                                                                     flash_bytes[i + 12], flash_bytes[i + 13], 
                                                                                                                                     flash_bytes[i + 14], flash_bytes[i + 15],
                                                                                                                                     flash_bytes[i + 16], flash_bytes[i + 17],
                                                                                                                                     flash_bytes[i + 18], flash_bytes[i + 19]);
    }
}
