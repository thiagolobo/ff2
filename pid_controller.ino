#include "pid_controller.h"

void ff2_pid_initialize()
{
    ff2_delay(INITIALIZATION_TIME);
    ff2_debug_printf(">> Initializing PID Controllers...\n");

    //
    // "Angular Speed" PIDs are NOT ready. Look through before using.
    //

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ===============================================================================================
    // Roll Angular Speed PID
    // ===============================================================================================
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ff2_pid_data angular_speed_roll;

    if (persistent_has_data)
    {   
        angular_speed_roll.KP = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_KP);
        angular_speed_roll.KI = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_KI);
        angular_speed_roll.KD = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_KD);
        angular_speed_roll.integral_constraint = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_INTEGRAL_CONSTRAINT);
        angular_speed_roll.output_constraint = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_OUTPUT_CONSTRAINT);
    }
    else
    {
        angular_speed_roll.KP = 2.0f;
        angular_speed_roll.KI = 0.0f;
        angular_speed_roll.KD = 0.1f; // 2.5 Hz Cutoff Frequency
        angular_speed_roll.integral_constraint = 80.0f;
        angular_speed_roll.output_constraint = 250.0f;

        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_KP, angular_speed_roll.KP);
        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_KI, angular_speed_roll.KI);
        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_KD, angular_speed_roll.KD);
        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_INTEGRAL_CONSTRAINT, angular_speed_roll.integral_constraint);
        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_ROLL_OUTPUT_CONSTRAINT, angular_speed_roll.output_constraint);
    }

    angular_speed_roll.use_setpoint_rate = false;
    angular_speed_roll.setpoint_LPF_time_constant = ff2_first_order_LPF_time_constant(2.5f); // 2.5 Hz Cutoff Frequency
    angular_speed_roll.setpoint_constraint = 80.0f;
    angular_speed_roll.derivative_LPF_time_constant = ff2_first_order_LPF_time_constant(10.0f);

    ff2_pid_array[PID_ANGULAR_SPEED_ROLL] = angular_speed_roll;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ===============================================================================================
    // Pitch Angular Speed PID
    // ===============================================================================================
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ff2_pid_data angular_speed_pitch;

    if (persistent_has_data)
    {   
        angular_speed_pitch.KP = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_KP);
        angular_speed_pitch.KI = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_KI);
        angular_speed_pitch.KD = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_KD);
        angular_speed_pitch.integral_constraint = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_INTEGRAL_CONSTRAINT);
        angular_speed_pitch.output_constraint = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_OUTPUT_CONSTRAINT);
    }
    else
    {
        angular_speed_pitch.KP = 2.0f;
        angular_speed_pitch.KI = 0.0f;
        angular_speed_pitch.KD = 0.1f; // 2.5 Hz Cutoff Frequency
        angular_speed_pitch.integral_constraint = 80.0f;
        angular_speed_pitch.output_constraint = 250.0f;

        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_KP, angular_speed_pitch.KP);
        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_KI, angular_speed_pitch.KI);
        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_KD, angular_speed_pitch.KD);
        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_INTEGRAL_CONSTRAINT, angular_speed_pitch.integral_constraint);
        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_PITCH_OUTPUT_CONSTRAINT, angular_speed_pitch.output_constraint);
    }

    angular_speed_pitch.use_setpoint_rate = false;
    angular_speed_pitch.setpoint_LPF_time_constant = ff2_first_order_LPF_time_constant(2.5f); // 2.5 Hz Cutoff Frequency
    angular_speed_pitch.setpoint_constraint = 80.0f;
    angular_speed_pitch.derivative_LPF_time_constant = ff2_first_order_LPF_time_constant(10.0f);

    ff2_pid_array[PID_ANGULAR_SPEED_PITCH] = angular_speed_pitch;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ===============================================================================================
    // Yaw Angular Speed PID
    // ===============================================================================================
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ff2_pid_data angular_speed_yaw;

    if (persistent_has_data)
    {   
        angular_speed_yaw.KP = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_KP);
        angular_speed_yaw.KI = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_KI);
        angular_speed_yaw.KD = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_KD);
        angular_speed_yaw.integral_constraint = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_INTEGRAL_CONSTRAINT);
        angular_speed_yaw.output_constraint = ff2_persistent_read_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_OUTPUT_CONSTRAINT);
    }
    else
    {
        angular_speed_yaw.KP = 1.0f;
        angular_speed_yaw.KI = 0.0f;
        angular_speed_yaw.KD = 0.0f; // 2.5 Hz Cutoff Frequency
        angular_speed_yaw.integral_constraint = 0.0f;
        angular_speed_yaw.output_constraint = 250.0f;

        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_KP, angular_speed_yaw.KP);
        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_KI, angular_speed_yaw.KI);
        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_KD, angular_speed_yaw.KD);
        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_INTEGRAL_CONSTRAINT, angular_speed_yaw.integral_constraint);
        ff2_persistent_write_float(PERSISTENT_OFFSET_PID_ANGULAR_SPEED_YAW_OUTPUT_CONSTRAINT, angular_speed_yaw.output_constraint);
    }

    angular_speed_yaw.use_setpoint_rate = false;
    angular_speed_yaw.setpoint_LPF_time_constant = ff2_first_order_LPF_time_constant(2.5f); // 2.5 Hz Cutoff Frequency
    angular_speed_yaw.setpoint_constraint = 80.0f;
    angular_speed_yaw.derivative_LPF_time_constant = ff2_first_order_LPF_time_constant(10.0f);

    ff2_pid_array[PID_ANGULAR_SPEED_YAW] = angular_speed_yaw;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ===============================================================================================
    // Roll Angle PID
    // ===============================================================================================
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ff2_pid_data angle_roll;

    
    angle_roll.KP = 2.5f;
    
    angle_roll.KI = 0.3f;
    angle_roll.integral_constraint = 100.0f;
    
    angle_roll.KD = 0.15f;
    angle_roll.derivative_LPF_time_constant = ff2_first_order_LPF_time_constant(10.0f);
    
    angle_roll.use_rate_feedback = true;
    angle_roll.balance_factor = 5.2f;    
    
    angle_roll.output_constraint = 200.0f;        
    
    angle_roll.use_setpoint_rate = true;
    angle_roll.setpoint_LPF_time_constant = ff2_first_order_LPF_time_constant(2.5f);
    angle_roll.setpoint_constraint = 80.0f;
    

    ff2_pid_array[PID_ANGLE_ROLL] = angle_roll;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ===============================================================================================
    // Pitch Angle PID
    // ===============================================================================================
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ff2_pid_data angle_pitch;

    
    angle_pitch.KP = 2.5f;
    
    angle_pitch.KI = 0.3f;
    angle_pitch.integral_constraint = 100.0f;
    
    angle_pitch.KD = 0.15f;
    angle_pitch.derivative_LPF_time_constant = ff2_first_order_LPF_time_constant(10.0f);
    
    angle_pitch.use_rate_feedback = true;
    angle_pitch.balance_factor = 5.2f;    
    
    angle_pitch.output_constraint = 200.0f;        
    
    angle_pitch.use_setpoint_rate = true;
    angle_pitch.setpoint_LPF_time_constant = ff2_first_order_LPF_time_constant(2.5f);
    angle_pitch.setpoint_constraint = 80.0f;
    

    ff2_pid_array[PID_ANGLE_PITCH] = angle_pitch;

    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // ===============================================================================================
    // Yaw Angle PID
    // ===============================================================================================
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    ff2_pid_data angle_yaw;

    
    angle_yaw.KP = 7.0f;
    
    angle_yaw.KI = 0.0f;
    angle_yaw.integral_constraint = 30.0f;
    
    angle_yaw.KD = 2.0f;
    angle_yaw.derivative_LPF_time_constant = ff2_first_order_LPF_time_constant(10.0f);
    
    angle_yaw.use_rate_feedback = true;
    angle_yaw.balance_factor = 5.2f;
    
    angle_yaw.output_constraint = 200.0f;        
    
    angle_yaw.use_setpoint_rate = true;
    angle_yaw.setpoint_LPF_time_constant = ff2_first_order_LPF_time_constant(2.5f);
    angle_yaw.setpoint_constraint = 80.0f;
    

    ff2_pid_array[PID_ANGLE_YAW] = angle_yaw;

}

float ff2_pid_run(byte pid_id, float setpoint, float measurement, float measurement_rate, float dt)
{
    ff2_pid_data* pid = &ff2_pid_array[pid_id];

    float error = 0.0f;

    if (pid->use_setpoint_rate)
    {
        // Setpoint Rate LPF pass
        pid->setpoint_rate = (setpoint - pid->last_setpoint + pid->setpoint_LPF_time_constant * pid->setpoint_rate) / (pid->setpoint_LPF_time_constant + dt);
        pid->last_setpoint = setpoint;
        pid->setpoint_rate = ff2_constrainf(pid->setpoint_rate, -pid->setpoint_constraint, pid->setpoint_constraint);
        error += pid->setpoint_rate;
    }

    
    if (pid->use_rate_feedback)
    {
        error += (setpoint - measurement) * pid->balance_factor - measurement_rate;
    }
    else
    {
        error += setpoint - measurement;
    }

    pid->integral += error * pid->KI * dt;
    pid->integral = ff2_constrainf(pid->integral, -pid->integral_constraint, pid->integral_constraint);

    // Derivative LPF pass
    if (pid->KD > 0.0f)
    {
        pid->derivative = (error - pid->last_error + pid->derivative_LPF_time_constant * pid->derivative) / (pid->derivative_LPF_time_constant + dt);
        pid->last_error = error;
    }
    else 
    {
        pid->derivative = 0.0f;
    }    

    float output = pid->KP * error + pid->integral + pid->KD * pid->derivative;
    
    return ff2_constrainf(output, -pid->output_constraint, pid->output_constraint);
}
