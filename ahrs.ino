#include "ahrs.h"

void ff2_ahrs_initialize()
{
    ff2_delay(INITIALIZATION_TIME);
    ff2_debug_printf(">> Initializing AHRS...\n");

    if (persistent_has_data)
    {   
        ahrs_kp_target = ff2_persistent_read_float(PERSISTENT_OFFSET_AHRS_KP);
        ahrs_ki_target = ff2_persistent_read_float(PERSISTENT_OFFSET_AHRS_KI);
    }
    else
    {
        ff2_persistent_write_float(PERSISTENT_OFFSET_AHRS_KP, ahrs_kp_target);
        ff2_persistent_write_float(PERSISTENT_OFFSET_AHRS_KI, ahrs_ki_target);
    }

    ff2_moving_average_initialize(&mean_deviation_roll, AHRS_MEAN_DEVIATION_SIZE);
    ff2_moving_average_initialize(&mean_deviation_pitch, AHRS_MEAN_DEVIATION_SIZE);
    ff2_moving_average_initialize(&mean_deviation_yaw, AHRS_MEAN_DEVIATION_SIZE);

    ff2_debug_printf("AHRS Convergence started. Targets KP = %f KI = %f...\n", ahrs_kp_target, ahrs_ki_target);
    ahrs_ki = ahrs_ki_target; // There is no need to converge to KI target.
    ff2_ahrs_compute_rotation_matrix();
}

void ff2_ahrs_compute_rotation_matrix()
{
    float q1q1 = FF2_SQR(ahrs_attitude_quaternion.x);
    float q2q2 = FF2_SQR(ahrs_attitude_quaternion.y);
    float q3q3 = FF2_SQR(ahrs_attitude_quaternion.z);

    float q0q1 = ahrs_attitude_quaternion.w * ahrs_attitude_quaternion.x;
    float q0q2 = ahrs_attitude_quaternion.w * ahrs_attitude_quaternion.y;
    float q0q3 = ahrs_attitude_quaternion.w * ahrs_attitude_quaternion.z;
    float q1q2 = ahrs_attitude_quaternion.x * ahrs_attitude_quaternion.y;
    float q1q3 = ahrs_attitude_quaternion.x * ahrs_attitude_quaternion.z;
    float q2q3 = ahrs_attitude_quaternion.y * ahrs_attitude_quaternion.z;

    ahrs_rotation_matrix[0][0] = 1.0f - 2.0f * q2q2 - 2.0f * q3q3;
    ahrs_rotation_matrix[0][1] = 2.0f * (q1q2 + -q0q3);
    ahrs_rotation_matrix[0][2] = 2.0f * (q1q3 - -q0q2);

    ahrs_rotation_matrix[1][0] = 2.0f * (q1q2 - -q0q3);
    ahrs_rotation_matrix[1][1] = 1.0f - 2.0f * q1q1 - 2.0f * q3q3;
    ahrs_rotation_matrix[1][2] = 2.0f * (q2q3 + -q0q1);

    ahrs_rotation_matrix[2][0] = 2.0f * (q1q3 + -q0q2);
    ahrs_rotation_matrix[2][1] = 2.0f * (q2q3 - -q0q1);
    ahrs_rotation_matrix[2][2] = 1.0f - 2.0f * q1q1 - 2.0f * q2q2;
}

void ff2_ahrs_euler_update()
{
    ff2_attitude.roll = ff2_atan2(ahrs_rotation_matrix[2][1], ahrs_rotation_matrix[2][2]) * (180.0f / FF2_PI);
    ff2_attitude.pitch = ((0.5f * FF2_PI) - ff2_acos(-ahrs_rotation_matrix[2][0])) * (180.0f / FF2_PI);
    ff2_attitude.yaw = ff2_atan2(ahrs_rotation_matrix[1][0], ahrs_rotation_matrix[0][0]) * (180.0f / FF2_PI) + MAGNETIC_DECLINATION;
}

void ff2_ahrs_mahony_update(float gx, float gy, float gz,
                            bool use_accel, float ax, float ay, float az,
                            bool use_compass, float mx, float my, float mz,
                            bool use_yaw, float yaw_error,
                            float dt)
{
    float recipNorm;
    float hx, hy, bx;
    float ex = 0.0f, ey = 0.0f, ez = 0.0f;
    float qa, qb, qc;

    // Calculate general spin rate (rad/s)
    float spin_rate = ff2_sqrt(FF2_SQR(gx) + FF2_SQR(gy) + FF2_SQR(gz));

    // Use raw heading error (from GPS or whatever else)
    if (use_yaw) 
    {
        while (yaw_error >  FF2_PI) yaw_error -= (FF2_TAU);
        while (yaw_error < -FF2_PI) yaw_error += (FF2_TAU);

        ez += ff2_sin(yaw_error / 2.0f);
    }

    // Use measured magnetic field vector
    recipNorm = FF2_SQR(mx) + FF2_SQR(my) + FF2_SQR(mz);
    if (use_compass && recipNorm > 0.01f) 
    {
        // Normalise magnetometer measurement
        recipNorm = ff2_inv_sqrt(recipNorm);
        mx *= recipNorm;
        my *= recipNorm;
        mz *= recipNorm;

        // For magnetometer correction we make an assumption that magnetic field is perpendicular to gravity (ignore Z-component in EF).
        // This way magnetic field will only affect heading and wont mess roll/pitch angles

        // (hx; hy; 0) - measured mag field vector in EF (assuming Z-component is zero)
        // (bx; 0; 0) - reference mag field vector heading due North in EF (assuming Z-component is zero)
        hx = ahrs_rotation_matrix[0][0] * mx + ahrs_rotation_matrix[0][1] * my + ahrs_rotation_matrix[0][2] * mz;
        hy = ahrs_rotation_matrix[1][0] * mx + ahrs_rotation_matrix[1][1] * my + ahrs_rotation_matrix[1][2] * mz;
        bx = ff2_sqrt(hx * hx + hy * hy);

        // magnetometer error is cross product between estimated magnetic north and measured magnetic north (calculated in EF)
        float ez_ef = -(hy * bx);

        // Rotate mag error vector back to BF and accumulate
        ex += ahrs_rotation_matrix[2][0] * ez_ef;
        ey += ahrs_rotation_matrix[2][1] * ez_ef;
        ez += ahrs_rotation_matrix[2][2] * ez_ef;
    }

    // Use measured acceleration vector
    recipNorm = FF2_SQR(ax) + FF2_SQR(ay) + FF2_SQR(az);
    if (use_accel && recipNorm > 0.01f) 
    {
        // Normalise accelerometer measurement
        recipNorm = ff2_inv_sqrt(recipNorm);
        ax *= recipNorm;
        ay *= recipNorm;
        az *= recipNorm;

        // Error is sum of cross product between estimated direction and measured direction of gravity
        ex += (ay * ahrs_rotation_matrix[2][2] - az * ahrs_rotation_matrix[2][1]);
        ey += (az * ahrs_rotation_matrix[2][0] - ax * ahrs_rotation_matrix[2][2]);
        ez += (ax * ahrs_rotation_matrix[2][1] - ay * ahrs_rotation_matrix[2][0]);
    }

    // Compute and apply integral feedback if enabled
    if(ahrs_ki > 0.0f) 
    {
        // Stop integrating if spinning beyond the certain limit
        if (spin_rate < FF2_RAD(AHRS_SPIN_RATE_LIMIT)) 
        {
            ahrs_integralFBx += ahrs_ki * ex * dt;    // integral error scaled by Ki
            ahrs_integralFBy += ahrs_ki * ey * dt;
            ahrs_integralFBz += ahrs_ki * ez * dt;
        }
    }
    else 
    {
        ahrs_integralFBx = 0.0f; // prevent integral windup
        ahrs_integralFBy = 0.0f;
        ahrs_integralFBz = 0.0f;
    }

    // Apply proportional and integral feedback
    gx += ahrs_kp * ex + ahrs_integralFBx;
    gy += ahrs_kp * ey + ahrs_integralFBy;
    gz += ahrs_kp * ez + ahrs_integralFBz;
    
    // Integrate rate of change of quaternion
    gx *= (0.5f * dt);
    gy *= (0.5f * dt);
    gz *= (0.5f * dt);

    qa = ahrs_attitude_quaternion.w;
    qb = ahrs_attitude_quaternion.x;
    qc = ahrs_attitude_quaternion.y;
    ahrs_attitude_quaternion.w += (-qb * gx - qc * gy - ahrs_attitude_quaternion.z * gz);
    ahrs_attitude_quaternion.x += (qa * gx + qc * gz - ahrs_attitude_quaternion.z * gy);
    ahrs_attitude_quaternion.y += (qa * gy - qb * gz + ahrs_attitude_quaternion.z * gx);
    ahrs_attitude_quaternion.z += (qa * gz + qb * gy - qc * gx);

    // Normalise quaternion
    recipNorm = ff2_inv_sqrt(FF2_SQR(ahrs_attitude_quaternion.w) + FF2_SQR(ahrs_attitude_quaternion.x) + FF2_SQR(ahrs_attitude_quaternion.y) + FF2_SQR(ahrs_attitude_quaternion.z));
    ahrs_attitude_quaternion.w *= recipNorm;
    ahrs_attitude_quaternion.x *= recipNorm;
    ahrs_attitude_quaternion.y *= recipNorm;
    ahrs_attitude_quaternion.z *= recipNorm;

    // Pre-compute rotation matrix from quaternion
    ff2_ahrs_compute_rotation_matrix();
}

void ff2_ahrs_update(float dt)
{
    ff2_ahrs_mahony_update(FF2_RAD(ff2_gyro_data.x), FF2_RAD(ff2_gyro_data.y), FF2_RAD(ff2_gyro_data.z),
                           true, ff2_accel_data.x, ff2_accel_data.y, ff2_accel_data.z,
                           false, ff2_compass_data.x, ff2_compass_data.y, ff2_compass_data.z,
                           false, 0.0f,
                           dt);
    
    ff2_ahrs_euler_update();
    ff2_ahrs_acceleration_update();

    if (!ff2_ahrs_ready)
    {
        if (ahrs_kp <= ahrs_kp_target + 0.01f 
            && mean_deviation_roll.average < AHRS_MEAN_DEVIATION_THRESHOLD
            && mean_deviation_pitch.average < AHRS_MEAN_DEVIATION_THRESHOLD
            && mean_deviation_yaw.average < AHRS_MEAN_DEVIATION_THRESHOLD)
        {
            ahrs_kp = ahrs_kp_target;
            ff2_debug_printf("AHRS locked! KP = %f KI = %f MDR = %f MDP = %f MDY = %f\n", ahrs_kp, ahrs_ki, mean_deviation_roll.average, mean_deviation_pitch.average, mean_deviation_yaw.average);            
            // No need to use memory after convergence is finished.
            ff2_moving_average_destroy(&mean_deviation_roll);
            ff2_moving_average_destroy(&mean_deviation_pitch);
            ff2_moving_average_destroy(&mean_deviation_yaw);
            ff2_ahrs_ready = true;
        }

        if (ahrs_kp > ahrs_kp_target + 0.01f)
        {
            ahrs_kp -= 0.1f;
        }        

        // 2 Hz
        if (ff2_synchronizer % 100 == 0)
        {
            ff2_moving_average_push(&mean_deviation_roll, FF2_ABS(ff2_attitude.roll - last_attitude.roll));
            ff2_moving_average_push(&mean_deviation_pitch, FF2_ABS(ff2_attitude.pitch - last_attitude.pitch));
            ff2_moving_average_push(&mean_deviation_yaw, FF2_ABS(ff2_attitude.yaw - last_attitude.yaw));

            last_attitude.roll = ff2_attitude.roll;
            last_attitude.pitch = ff2_attitude.pitch;
            last_attitude.yaw = ff2_attitude.yaw;    
        }
    }
}

void ff2_ahrs_acceleration_update()
{
    ff2_vector3f acceleration_body;

    acceleration_body.x = ff2_accel_data.x;
    acceleration_body.y = ff2_accel_data.y;
    acceleration_body.z = ff2_accel_data.z;

    ff2_rotate_vector(ahrs_rotation_matrix, &acceleration_body);
    
    ff2_accel_data_earth.x = acceleration_body.x;
    ff2_accel_data_earth.y = acceleration_body.y;
    ff2_accel_data_earth.z = acceleration_body.z;
}

void ff2_ahrs_set_kp(float value)
{
    ahrs_kp = value;
    ff2_persistent_write_float(PERSISTENT_OFFSET_AHRS_KP, ahrs_kp);
}

void ff2_ahrs_set_ki(float value)
{
    ahrs_ki = value;
    ff2_persistent_write_float(PERSISTENT_OFFSET_AHRS_KI, ahrs_ki);
}