#include "mpu6050.h"

void ff2_mpu6050_initialize()
{
    ff2_delay(INITIALIZATION_TIME);
    ff2_debug_printf(">> Initializing MPU6050...\n");

    ff2_make_output(MPU6050_PIN_GND);
    ff2_output_mode(MPU6050_PIN_GND, OFF);

    ff2_moving_average_initialize(&gyro_x, MPU6050_GYRO_AVG_SIZE);
    ff2_moving_average_initialize(&gyro_y, MPU6050_GYRO_AVG_SIZE);
    ff2_moving_average_initialize(&gyro_z, MPU6050_GYRO_AVG_SIZE);
    ff2_moving_average_initialize(&accel_x, MPU6050_ACCEL_AVG_SIZE);
    ff2_moving_average_initialize(&accel_y, MPU6050_ACCEL_AVG_SIZE);
    ff2_moving_average_initialize(&accel_z, MPU6050_ACCEL_AVG_SIZE);

    ff2_i2cd_write(MPU6050_ADDRESS, 0x6B, 0x80); // Reset and disable temperature sensor
    ff2_delay(100);
    ff2_i2cd_write(MPU6050_ADDRESS, 0x6B, 0x03); // Gyro z as clock reference
    ff2_delay(100);
    ff2_i2cd_write(MPU6050_ADDRESS, 0x19, 0x00); // Sample rate = 1kHz 
    ff2_delay(100);
    ff2_i2cd_write(MPU6050_ADDRESS, 0x1A, 0x02); // Bandwidth = 42Hz
    ff2_delay(100);
    ff2_i2cd_write(MPU6050_ADDRESS, 0x1B, 0x10); // 1000dps gyro - Bits 4 & 3 -> 11 2000; 10 1000; 01 500; 00 250
    ff2_delay(100);
    ff2_i2cd_write(MPU6050_ADDRESS, 0x1C, 0x10); // 8g accel precision (4096 = 1g)
    ff2_delay(100);
    ff2_i2cd_write(MPU6050_ADDRESS, 0x37, 0x02); // i2c bypass
    ff2_delay(100);

    ff2_debug_printf(">> Callibrating Gyro...\n");

    float x_accum = 0.0f, y_accum = 0.0f, z_accum = 0.0f;

    for (unsigned i = 0; i < MPU6050_CALLIBRATION_SIZE; i++) 
    {
        // delay(5);
        ff2_i2cd_read(MPU6050_ADDRESS, 0x43, 6, i2cd_output_6x);

        float x_val = ((int16_t) ((i2cd_output_6x[0] << 8) | i2cd_output_6x[1])) * MPU6050_GYRO_SCALE_FACTOR - gyro_x_offset;
        float y_val = ((int16_t) ((i2cd_output_6x[2] << 8) | i2cd_output_6x[3])) * MPU6050_GYRO_SCALE_FACTOR - gyro_y_offset;
        float z_val = ((int16_t) ((i2cd_output_6x[4] << 8) | i2cd_output_6x[5])) * MPU6050_GYRO_SCALE_FACTOR - gyro_z_offset;    

        x_accum += x_val;
        y_accum += y_val;
        z_accum += z_val;

        // ff2_printf("%f %f %f\n", x_val, y_val, z_val);
    }

    gyro_x_offset = x_accum / MPU6050_CALLIBRATION_SIZE;
    gyro_y_offset = y_accum / MPU6050_CALLIBRATION_SIZE;
    gyro_z_offset = z_accum / MPU6050_CALLIBRATION_SIZE;  

    ff2_debug_printf(">> Gyro callibrated: \n%f\n%f\n%f\n", gyro_x_offset, gyro_y_offset, gyro_z_offset);

    // Check callibration values for sanity and if wrong, reset the MPU
    // if (FF2_ABS(gyro_x_offset) > 3.0f || FF2_ABS(gyro_y_offset) > 3.0f || FF2_ABS(gyro_z_offset) > 3.0f)
    // {
    //     ff2_debug_printf(">> Bad callibration detected! Will reset the MPU.\n");
    //     ff2_output_mode(MPU6050_PIN_GND, ON);
    //     delay(5000);
    //     ff2_mpu6050_initialize();
    // }
}

void ff2_mpu6050_update()
{
    ff2_i2cd_read(MPU6050_ADDRESS, 0x43, 6, i2cd_output_6x);

    // Enable the following for min/max tracking
    // float x_val = 0.0f;
    // float y_val = 0.0f;
    // float z_val = 0.0f;

    // x_val = ((int16_t) ((i2cd_output_6x[0] << 8) | i2cd_output_6x[1])) * MPU6050_GYRO_SCALE_FACTOR - gyro_x_offset;
    // y_val = ((int16_t) ((i2cd_output_6x[2] << 8) | i2cd_output_6x[3])) * MPU6050_GYRO_SCALE_FACTOR - gyro_y_offset;
    // z_val = ((int16_t) ((i2cd_output_6x[4] << 8) | i2cd_output_6x[5])) * MPU6050_GYRO_SCALE_FACTOR - gyro_z_offset;    

    // if (x_val < gyro_x_min) gyro_x_min = x_val;
    // if (y_val < gyro_y_min) gyro_y_min = y_val;
    // if (z_val < gyro_z_min) gyro_z_min = z_val;

    // if (x_val > gyro_x_max) gyro_x_max = x_val;
    // if (y_val > gyro_y_max) gyro_y_max = y_val;
    // if (z_val > gyro_z_max) gyro_z_max = z_val;

    // ff2_moving_average_push(&gyro_x, x_val);
    // ff2_moving_average_push(&gyro_y, y_val);
    // ff2_moving_average_push(&gyro_z, z_val);

    ff2_moving_average_push(&gyro_x, ((int16_t) ((i2cd_output_6x[0] << 8) | i2cd_output_6x[1])) * MPU6050_GYRO_SCALE_FACTOR - gyro_x_offset);
    ff2_moving_average_push(&gyro_y, ((int16_t) ((i2cd_output_6x[2] << 8) | i2cd_output_6x[3])) * MPU6050_GYRO_SCALE_FACTOR - gyro_y_offset);
    ff2_moving_average_push(&gyro_z, ((int16_t) ((i2cd_output_6x[4] << 8) | i2cd_output_6x[5])) * MPU6050_GYRO_SCALE_FACTOR - gyro_z_offset);

    ff2_gyro_data.x = gyro_x.average;
    ff2_gyro_data.y = gyro_y.average;
    ff2_gyro_data.z = gyro_z.average;

    ff2_i2cd_read(MPU6050_ADDRESS, 0x3B, 6, i2cd_output_6x);

    int ax = ((int16_t) ((i2cd_output_6x[0] << 8) | i2cd_output_6x[1]));
    int ay = ((int16_t) ((i2cd_output_6x[2] << 8) | i2cd_output_6x[3]));
    int az = ((int16_t) ((i2cd_output_6x[4] << 8) | i2cd_output_6x[5]));

    // Enable the following for min/max tracking
    // x_val = ff2_range_mapf(ax, MPU6050_ACCEL_X_MIN, MPU6050_ACCEL_X_MAX, -1.0f, 1.0f) - MPU6050_ACCEL_X_TRIM;
    // y_val = ff2_range_mapf(ay, MPU6050_ACCEL_Y_MIN, MPU6050_ACCEL_Y_MAX, -1.0f, 1.0f) - MPU6050_ACCEL_Y_TRIM;
    // z_val = ff2_range_mapf(az, MPU6050_ACCEL_Z_MIN, MPU6050_ACCEL_Z_MAX, -1.0f, 1.0f) - MPU6050_ACCEL_Z_TRIM;    

    // if (x_val < accel_x_min) accel_x_min = x_val;
    // if (y_val < accel_y_min) accel_y_min = y_val;
    // if (z_val < accel_z_min) accel_z_min = z_val;

    // if (x_val > accel_x_max) accel_x_max = x_val;
    // if (y_val > accel_y_max) accel_y_max = y_val;
    // if (z_val > accel_z_max) accel_z_max = z_val;

    // ff2_moving_average_push(&accel_x, x_val);
    // ff2_moving_average_push(&accel_y, y_val);
    // ff2_moving_average_push(&accel_z, z_val);

    ff2_moving_average_push(&accel_x, ff2_range_mapf(ax, MPU6050_ACCEL_X_MIN, MPU6050_ACCEL_X_MAX, -1.0f, 1.0f) - MPU6050_ACCEL_X_TRIM);
    ff2_moving_average_push(&accel_y, ff2_range_mapf(ay, MPU6050_ACCEL_Y_MIN, MPU6050_ACCEL_Y_MAX, -1.0f, 1.0f) - MPU6050_ACCEL_Y_TRIM);
    ff2_moving_average_push(&accel_z, ff2_range_mapf(az, MPU6050_ACCEL_Z_MIN, MPU6050_ACCEL_Z_MAX, -1.0f, 1.0f) - MPU6050_ACCEL_Z_TRIM);

    ff2_accel_data.x = accel_x.average;
    ff2_accel_data.y = accel_y.average;
    ff2_accel_data.z = accel_z.average;
}
