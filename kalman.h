#include "maths.h"
/*

	Link is http://part.put.poznan.pl/articles/article.pdf

*/

#define T 			99.0f	//fix, T = time during iterations

/*
	This function has several restrictions:

	- measurement is a 2x1 array containing the height and acceleration values
		provided by the sensors, IN THIS ORDER.

	- last_estimate is an 3x1 array containing previsions regarding the model
		defined within the function, and contains values for the height, 
		acceleration, and velocity. IN THIS ORDER.

	- covar is a covariance matrix using the 3 estimated variables,
		height, acceleration and velocity.IN THIS ORDER.

*/

// float* ff2_kalman_filter (float* measurement, float* last_estimate, float* covar);


// float* ff2_generate_noise (void);