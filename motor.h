#pragma once

#include "maths.h"

#define MOTOR_COUNT 4

#define MOTOR_FRONT_L_PORT	6
#define MOTOR_FRONT_R_PORT	7
#define MOTOR_BACK_L_PORT	8
#define MOTOR_BACK_R_PORT	9

#define MOTOR_FRONT_L_ALIAS	7
#define MOTOR_FRONT_R_ALIAS	6
#define MOTOR_BACK_L_ALIAS	5
#define MOTOR_BACK_R_ALIAS	4

#define MOTOR_FRONT_L	3
#define MOTOR_FRONT_R	0
#define MOTOR_BACK_L	2
#define MOTOR_BACK_R	1

#define DEADZONE_OFFSET 		100
#define MOTOR_MIN_THROTTLE 		983
#define MOTOR_START_THROTTLE 	1024
#define MOTOR_MAX_THROTTLE 		2460

typedef struct ff2_motor_def
{
	byte port;
	byte alias;
	int throttle;
} ff2_motor;

ff2_motor motors[MOTOR_COUNT];

void ff2_motor_configure(ff2_motor* motor);
void ff2_motor_initialize();
void ff2_motor_set_throttle(byte motor, int throttle);
