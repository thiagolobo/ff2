#include "Arduino.h"

#include "Wire_due32.h"

#include "arduino_wrapper.h"
#include "maths.h"
#include "behavior.h"

void setup()
{
	ff2_behavior_initialize();
}

void loop()
{	
	ff2_behavior_update();
}
