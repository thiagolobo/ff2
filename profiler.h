#pragma once

#include "arduino_wrapper.h"

unsigned long profiler_count;
unsigned long profiler_last_time;
unsigned long profiler_accumulated;

void ff2_profiler_start();
void ff2_profiler_update();
void ff2_profiler_end();
