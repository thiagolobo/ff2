#pragma once

#include "arduino_wrapper.h"
#include "i2c_device.h"
#include "maths.h"

#define MPU6050_ADDRESS 0x68

#define MPU6050_PIN_GND	24

#define MPU6050_GYRO_SCALE_FACTOR (1000.0f / 32768.0f)

#define MPU6050_GYRO_AVG_SIZE   	3
#define MPU6050_ACCEL_AVG_SIZE  	15
#define MPU6050_TEMP_AVG_SIZE		2
#define MPU6050_CALLIBRATION_SIZE 	2000

// Should measure this...
#define MPU6050_ACCEL_X_MIN -4160
#define MPU6050_ACCEL_X_MAX  3968
#define MPU6050_ACCEL_Y_MIN -4137
#define MPU6050_ACCEL_Y_MAX  4063
#define MPU6050_ACCEL_Z_MIN -4216
#define MPU6050_ACCEL_Z_MAX  4007

#define MPU6050_ACCEL_X_TRIM 0.0472f
#define MPU6050_ACCEL_Y_TRIM -0.003f
#define MPU6050_ACCEL_Z_TRIM 0.0146f

#define MPU6050_DEBUG_LENGTH 5120
int debug_counter = 0;
float debug_values[MPU6050_DEBUG_LENGTH];

ff2_moving_average gyro_x;
ff2_moving_average gyro_y;
ff2_moving_average gyro_z;
ff2_moving_average gyro_temperature;

ff2_vector3f ff2_gyro_data;

ff2_moving_average accel_x;
ff2_moving_average accel_y;
ff2_moving_average accel_z;

ff2_vector3f ff2_accel_data;

float gyro_x_offset;
float gyro_y_offset;
float gyro_z_offset;

float gyro_x_min = 0.0f;
float gyro_y_min = 0.0f;
float gyro_z_min = 0.0f;

float gyro_x_max = 0.0f;
float gyro_y_max = 0.0f;
float gyro_z_max = 0.0f;

float accel_x_min = 0.0f;
float accel_y_min = 0.0f;
float accel_z_min = 0.0f;

float accel_x_max = 0.0f;
float accel_y_max = 0.0f;
float accel_z_max = 0.0f;

bool ff2_mpu6050_ready = false;

void ff2_mpu6050_initialize();
void ff2_mpu6050_update();
