#include "moving_average.h"

void ff2_moving_average_initialize(ff2_moving_average* data, unsigned size)
{
	data->size = size;
	data->values = (float*) malloc(sizeof(float) * size);
}

void ff2_moving_average_push(ff2_moving_average* data, float value)
{
	data->sum += value;

	if (!(data->entry_counter < data->size)) 
	{
		data->sum -= data->values[(data->entry_counter - data->size) % data->size];
	}

	data->values[data->entry_counter++ % data->size] = value;

	data->average = data->sum / ((data->entry_counter >= data->size) ? data->size : data->entry_counter);

	// data->average -= data->average / data->size;
 	// data->average += value / data->size;
}

void ff2_moving_average_debug()
{
	ff2_moving_average average;
	
	ff2_moving_average_initialize(&average, 10);

	ff2_moving_average_push(&average, 10.0);
	ff2_debug_printf("%f\n", average.average);
	
	ff2_moving_average_push(&average, 5.0);
	ff2_debug_printf("%f\n", average.average);
	
	ff2_moving_average_push(&average, 7.5);
	ff2_debug_printf("%f\n", average.average);
	
	ff2_moving_average_push(&average, 10.0);
	ff2_debug_printf("%f\n", average.average);
	
	ff2_moving_average_push(&average, 5.0);
	ff2_debug_printf("%f\n", average.average);

	ff2_moving_average_push(&average, -7.5);
	ff2_debug_printf("%f\n", average.average);

	ff2_moving_average_push(&average, -7.5);
	ff2_debug_printf("%f\n", average.average);

	ff2_moving_average_push(&average, -7.5);
	ff2_debug_printf("%f\n", average.average);

	ff2_moving_average_push(&average, -7.5);
	ff2_debug_printf("%f\n", average.average);

	ff2_moving_average_push(&average, -7.5);
	ff2_debug_printf("%f\n", average.average);

	ff2_moving_average_push(&average, 5.0);
	ff2_debug_printf("%f\n", average.average);

	ff2_moving_average_push(&average, FF2_PI);
	ff2_debug_printf("%f\n", average.average);

	ff2_moving_average_destroy(&average);
}

void ff2_moving_average_destroy(ff2_moving_average* data)
{
	free(data->values);
}