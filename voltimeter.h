#pragma once

#include "arduino_wrapper.h"
#include "persistent.h"
#include "blackbox.h"
#include "moving_average.h"

#define VOLTIMETER_PORT	A0

#define VOLTIMETER_R1 390.0f
#define VOLTIMETER_R2 100.0f
#define VOLTIMETER_VOLTAGE_DIVIDER_INVERSE_GAIN ((VOLTIMETER_R1 + VOLTIMETER_R2) / VOLTIMETER_R2)
#define VOLTIMETER_CORRECTION_FACTOR 1.04f
#define VOLTIMETER_AVERAGE_SIZE 10

ff2_moving_average voltimeter_average;

float voltimeter_threshold = 11.4f;
int voltimeter_bbp = 2000; //bbp = blackbox period

void ff2_voltimeter_initialize();
void ff2_voltimeter_update();
void ff2_voltimeter_debug();
void ff2_voltimeter_set_threshold(float value);
void ff2_voltimeter_set_bbp(int value);
