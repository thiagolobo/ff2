#include "i2c_device.h"

void ff2_i2cd_initialize()
{
	ff2_delay(INITIALIZATION_TIME);
	ff2_i2c_begin();
	i2cd_output_6x = (int*) malloc(sizeof(int) * 6);
}

void ff2_i2cd_write(int device_address, byte register_address, byte value)
{
	ff2_i2c_begin_transmission(device_address);
	ff2_i2c_write(register_address);
	ff2_i2c_write(value);
	ff2_i2c_end_transmission();
}

// void ff2_i2cd_write(int device_address, byte base_register_address, unsigned length, int* vector)
// {

// }

void ff2_i2cd_read(int device_address, byte base_register_address, unsigned length, int* vector)
{
	ff2_i2c_begin_transmission(device_address);
	ff2_i2c_write(base_register_address);
	ff2_i2c_end_transmission();

	ff2_i2c_request(device_address, length);

	int i = 0;

	while (ff2_i2c_available())
	{
		vector[i++] = ff2_i2c_read();
		if (i > length)
		{
			ff2_debug_printf("I2C failure!\n");
		}		
	}

	ff2_i2c_end_transmission();
}

void ff2_i2cd_read(int device_address, byte register_address, int* value)
{
	ff2_i2c_begin_transmission(device_address);
	ff2_i2c_write(register_address);
	ff2_i2c_end_transmission();

	ff2_i2c_request(device_address, 1);

	while (ff2_i2c_available())
	{
		*value = ff2_i2c_read();
	}

	ff2_i2c_end_transmission();
}
