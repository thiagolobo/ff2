#README#
***
This file will hold all patterns used in the source code, in order to guide our development.

#DEFINE#
***
All defines hold the rule 
> `#define COMPONENT_NAME`

Where COMPONENT is the components identification, and NAME is the defined name for that value. Always try to be as clear as possible regarding the defined name, using underscores to separate composite names.

> `#define COMPONENT_COMPOSITE1_COMPOSITE2`

Try to use other previously used defines.
> `#define PERIOD (1.0f / BEHAVIOR_UPDATE_RATE)`

Where BEHAVIOR_UPDATE_RATE is a define used in another header file.

# VARIABLES #
***

## Global Variables ##
Global variables that are used by other functions defined in other files MUST be followed by `ff2_`
> `float ff2_altitude`

If the global variable is used only within the same file, it must **NOT** be followed by `ff2_`.
> `float covariance_matrix[2][2]`

Always trying to be as clear as possible with their nomenclature. Also, they must be lowercase and have their composite names separated by underscores.
>`float variable_name1_name2`

## Local Variables ##
Same rules as Global variables, except that it must **NOT** be followed by `ff2_`.

## Volatile Pointers
They should be declared using malloc.

````
float volatile_variable = (float*) malloc(sizeof(float)*SIZE);
{
   *code...*
}

free(volatile_variable);
````


# FUNCTIONS #
***
All function names must be followed by `ff2_`.
> `ff2_FUNCTION_NAME()`

Using Arduino functions is *strictly* forbidden, always use wrapper functions to make the code as generic as possible regarding the platform to be used. All wrappers must be defined inside `arduino_wrapper.h`

# FORMATTING #
***

##Braces
All braces must be on the following line:

````
ff2_func()
{
}
````
##Floats
All floats must have a trailing `.Xf`. Also do **NOT** use `double`.

> `float a = 1.0f`

> `float b = 2.3f`

##Arrays
Do not declare arrays using `malloc`, only directly.

> `float array[NUMBER_ELEMENTS]`

> `float array[NUMBER_ROWS][NUMBER_COLS]`

## Equations
Must have spaces to improve readability.
>`a + b`

## Synchronizer
All synchronized tasks use the global variable `ff2_synchronizer`, which increases by 200 each second.

> `ff2_synchronizer % 100 == 0` Creates a 2Hz task.